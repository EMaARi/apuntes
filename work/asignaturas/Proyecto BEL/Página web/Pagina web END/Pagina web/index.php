<?php
    $usuario = $_GET['u'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <!-- Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <meta charset="UTF-8">
    <title>BIBLIOTECA EMB&CO</title>
    <link type="text/css" rel="stylesheet" href="css/general.css">



    <script type="text/javascript" src="javascript/main.js"></script>
  </head>
  <body>
<!--///////////////////////////////////////////////////////////////////////////////////////////-->

    <header>
        <center>
            <nav class="navegar">
                <ul class="menu">
                
     
              
                    <li><a href="#"> Home </a></li>
    
                    <li> <a href="#">Tipo de libros </a>
                         <ul class="submenu">
                            <li><a href="#"> Adultos </a></li>
                            <li><a href="#"> Adolescentes </a></li>
                            <li><a href="#"> Infantil </a></li>
                        </ul>
                    </li>
                    <li><a href="#"> Categoria </a>
                        <ul class="submenu">
                            <li><a href="#"> Drama </a></li>
                            <li><a href="#"> Acción </a></li>
                            <li><a href="#"> Comedia </a></li>
                            <li><a href="#"> Romance </a></li>
                        </ul>
                    </li>
                    <li><a href="formulario.php?u=<?php echo $usuario ?>"> ¿Buscas un Libro? </a>
                       
                    </li>
    
   
                <li> <a class="list-group-item" href=""><i class="fa fa-user" aria-hidden="true"User:?php echo $usuario ?> <?php echo $usuario ?></i>&nbsp; </a>
                
                <ul class="submenu">
                            <li><a href="cerrar_login.php?u=<?php echo $usuario ?>">Sing in</a> </li>

                            <li><a href="cerrar_login.php?u=<?php echo $usuario ?>">cerrar sesion</a> </li>
                        </ul>

                    <li> <a class="list-group-item" href="#"><i class="fa fa-cog fa-fw" aria-hidden="true"></i>&nbsp; </a>
                        <ul class="submenu">
                            <li><a href="#"> Cuenta </a></li>
                            <li><a href="#"> Prestamo </a></li>
                            <li><a href="#"> Prestamo </a></li>
                </ul>
            </nav>
        </center>
    </header>
    <div id="linea"></div>

<!--///////////////////////////////////////////////////////////////////////////////////////////-->

    <div id="cuerpo">
        <div class="imagenes">
            <!--image slider start-->
            <div class="slider">
              <div class="slides">
                <h1>sss</h2>
                <!--radio buttons start-->
                <input type="radio" name="radio-btn" id="radio1">
                <input type="radio" name="radio-btn" id="radio2">
                <input type="radio" name="radio-btn" id="radio3">
                <input type="radio" name="radio-btn" id="radio4">
                <input type="radio" name="radio-btn" id="radio5">
                <!--radio buttons end-->
                <!--slide images start-->
                <div class="slide first">
                  <img src="content/book01.jpg" alt="">
                </div>
                <div class="slide">
                  <img src="content/book02.jpg" alt="">
                </div>
                <div class="slide">
                  <img src="content/book03.jpg" alt="">
                </div>
                <div class="slide">
                  <img src="content/book04.jpg" alt="">
                </div>
                <div class="slide">
                  <img src="content/book05.jpg" alt="">
                </div>
                <!--slide images end-->
                <!--automatic navigation start-->
                <div class="navigation-auto">
                  <div class="auto-btn1"></div>
                  <div class="auto-btn2"></div>
                  <div class="auto-btn3"></div>
                  <div class="auto-btn4"></div>
                  <div class="auto-btn5"></div>
                </div>
                <!--automatic navigation end-->
              </div>
              <!--manual navigation start-->
              <div class="navigation-manual">
                <label for="radio1" class="manual-btn"></label>
                <label for="radio2" class="manual-btn"></label>
                <label for="radio3" class="manual-btn"></label>
                <label for="radio4" class="manual-btn"></label>
                <label for="radio5" class="manual-btn"></label>
              </div>
              <!--manual navigation end-->
            </div>
            <!--image slider end-->
           
        </div>
        <a href="#">Learn More <i class="fas fa-angle-double-right"></i></a>
        

     <!-- NEWS CARDS -->
     <div class="news-cards">
      <div>
        <img src="img/news1.jpg" alt="" />
        <h3>Lorem, ipsum dolor.</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam dolore fugit esse corporis nesciunt minima
          doloremque modi mollitia rerum, similique optio eligendi itaque amet qui ullam vel incidunt asperiores fuga?
        </p>
        <a href="#">Learn More <i class="fas fa-angle-double-right"></i></a>
      </div>
      <div>
        <img src="img/news2.jpg" alt="" />
        <h3>Lorem, ipsum dolor.</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam dolore fugit esse corporis nesciunt minima
          doloremque modi mollitia rerum, similique optio eligendi itaque amet qui ullam vel incidunt asperiores fuga?
        </p>
        <a href="#">Learn More <i class="fas fa-angle-double-right"></i></a>
      </div>
      <div>
        <img src="img/news3.jpg" alt="" />
        <h3>Lorem, ipsum dolor.</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam dolore fugit esse corporis nesciunt minima
          doloremque modi mollitia rerum, similique optio eligendi itaque amet qui ullam vel incidunt asperiores fuga?
        </p>
        <a href="#">Learn More <i class="fas fa-angle-double-right"></i></a>
      </div>
 
    </div>
    
<!--///////////////////////////////////////////////////////////////////////////////////////////-->


    

<!--///////////////////////////////////////////////////////////////////////////////////////////-->



  <!-- Footer Links -->
  <div class="footer-links">
    <div class="footer-container">
      <ul>
        <li>
          <a href="#">
            <h3>Horarios de la Tienda</h3>
          </a>
        </li>
        <li>
          <a href="#">Entre Semana</a>
        </li>
     
        <li>
          <a href="#">7:30 14:30</a>
        </li>
        <li>
          <a href="#">15:30 20:45</a>
        </li>
        <li>
          <a href="#">Fines de semana </a>
        </li>
        <li>
          <a href="#">9:30 13:30</a>
        </li>
        <li>
          <a href="#">15:00 20:30</a>
        </li>
        <li>
          <a href="#">Para mas informacion para el Horarios | Localizacion de la BIBLIOTECA</a>
        </li>
      </ul>
      <ul>
        <li>
          <a href="#">
            <h3>Redes Sociales</h3>
          </a>
        </li>
        <li>
        <a href="https://www.instagram.com/">
                    <i class="fa fa-camera-retro fa-3x"></i></a>
          
        </li>
        <li>
        <a href="https://twitter.com/">
                    <i class="fab fa-twitter fa-3x"></i></a>
        </li>
        <li>
        <a href="https://www.facebook.com/">
                    <i class="fab fa-facebook-f fa-3x"></i>

        </li>
        <li>
        <a href="https://linkdin.com">
          <i class="fab fa-linkedin fa-3x"></i>
        </a>
        </li>
        
       
      </ul>
      <ul>
        <li>
          <a href="#">
            <h3>Title Three</h3>
          </a>
        </li>
        <li>
          <a href="#">Blockchain</a>
        </li>
        <li>
          <a href="#">Machine Learning</a>
        </li>
        <li>
          <a href="#">Artificial Intelligence</a>
        </li>
        <li>
          <a href="#">Quantum Computing</a>
        </li>
        <li>
          <a href="#">Startup</a>
        </li>
        <li>
          <a href="#">Cryptocurrencies</a>
        </li>
      </ul>
        <ul>
          <li>
            <a href="#">
              <h3> Biblioteca De la comunidad de Madrid </h3>
            </a>
          </li>
          <li>
            <a href="https://www.madrid.es/portales/munimadrid/es/Inicio/Cultura-ocio-y-deporte/Cultura-y-ocio/Direcciones-y-telefonos/Biblioteca-Publica-Municipal-Pablo-Neruda-Ciudad-Lineal-/?vgnextfmt=default&vgnextoid=d5c548d20675e010VgnVCM1000000b205a0aRCRD&vgnextchannel=76f3efff228fe410VgnVCM2000000c205a0aRCRD">El Portal del Lector</a>
          </li>
          <li>
            <a href="#">Machine Learning</a>
          </li>
          <li>
            <a href="#">Artificial Intelligence</a>
          </li>
          <li>
            <a href="#">Quantum Computing</a>
          </li>
       

        </ul>
    </div>
  </div>

  <!-- Footer -->
  <footer class="footer">
    <h3>BIBLIOTECA EMB & CO</h3>
  </footer>
  </body>
</html>

