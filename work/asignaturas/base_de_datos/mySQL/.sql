-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Coche`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Coche` (
  `idCoche` INT NOT NULL,
  `Nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`idCoche`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Puesto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Puesto` (
  `idPuesto` INT NOT NULL,
  `Descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`idPuesto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Empleado` (
  `idEmpleado` INT NOT NULL,
  `Nombre` VARCHAR(45) NULL,
  `Puesto_idPuesto` INT NOT NULL,
  PRIMARY KEY (`idEmpleado`),
  INDEX `fk_Empleado_Puesto1_idx` (`Puesto_idPuesto` ASC),
  CONSTRAINT `fk_Empleado_Puesto1`
    FOREIGN KEY (`Puesto_idPuesto`)
    REFERENCES `mydb`.`Puesto` (`idPuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Atiende`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Atiende` (
  `Coche_idCoche` INT NOT NULL,
  `Empleado_idEmpleado` INT NOT NULL,
  `Fecha` DATE NULL,
  PRIMARY KEY (`Coche_idCoche`, `Empleado_idEmpleado`),
  INDEX `fk_Coche_has_Empleado_Empleado1_idx` (`Empleado_idEmpleado` ASC),
  INDEX `fk_Coche_has_Empleado_Coche_idx` (`Coche_idCoche` ASC),
  CONSTRAINT `fk_Coche_has_Empleado_Coche`
    FOREIGN KEY (`Coche_idCoche`)
    REFERENCES `mydb`.`Coche` (`idCoche`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Coche_has_Empleado_Empleado1`
    FOREIGN KEY (`Empleado_idEmpleado`)
    REFERENCES `mydb`.`Empleado` (`idEmpleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
