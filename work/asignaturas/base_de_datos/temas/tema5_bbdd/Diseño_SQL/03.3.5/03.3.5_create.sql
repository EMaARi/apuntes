-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2021-02-10 09:19:20.882

-- tables
-- Table: Ingreso
CREATE TABLE Ingreso (
    codigo_ingreso char(10) NOT NULL,
    habitacion varchar(5) NOT NULL,
    fecha date NOT NULL,
    CONSTRAINT Ingreso_pk PRIMARY KEY (codigo_ingreso)
);

-- Table: Ingreso_Paciente
CREATE TABLE Ingreso_Paciente (
    codigo_proceso char(10) NOT NULL,
    Ingreso_codigo_ingreso char(10) NOT NULL,
    Paciente_codigo_paciente char(10) NOT NULL,
    CONSTRAINT Ingreso_Paciente_pk PRIMARY KEY (codigo_proceso)
);

-- Table: Medico
CREATE TABLE Medico (
    codigo_medico char(10) NOT NULL,
    nombre varchar(5) NOT NULL,
    apellidos varchar(5) NOT NULL,
    CONSTRAINT Medico_pk PRIMARY KEY (codigo_medico)
);

-- Table: Medico_Ingreso
CREATE TABLE Medico_Ingreso (
    codigo_atencion char(10) NOT NULL,
    Medico_codigo_medico char(10) NOT NULL,
    Ingreso_codigo_ingreso char(10) NOT NULL,
    CONSTRAINT Medico_Ingreso_pk PRIMARY KEY (codigo_atencion)
);

-- Table: Paciente
CREATE TABLE Paciente (
    codigo_paciente char(10) NOT NULL,
    nombre varchar(5) NOT NULL,
    apellidos varchar(5) NOT NULL,
    CONSTRAINT Paciente_pk PRIMARY KEY (codigo_paciente)
);

-- foreign keys
-- Reference: Ingreso_Paciente_Ingreso (table: Ingreso_Paciente)
ALTER TABLE Ingreso_Paciente ADD CONSTRAINT Ingreso_Paciente_Ingreso FOREIGN KEY Ingreso_Paciente_Ingreso (Ingreso_codigo_ingreso)
    REFERENCES Ingreso (codigo_ingreso);

-- Reference: Ingreso_Paciente_Paciente (table: Ingreso_Paciente)
ALTER TABLE Ingreso_Paciente ADD CONSTRAINT Ingreso_Paciente_Paciente FOREIGN KEY Ingreso_Paciente_Paciente (Paciente_codigo_paciente)
    REFERENCES Paciente (codigo_paciente);

-- Reference: Medico_Ingreso_Ingreso (table: Medico_Ingreso)
ALTER TABLE Medico_Ingreso ADD CONSTRAINT Medico_Ingreso_Ingreso FOREIGN KEY Medico_Ingreso_Ingreso (Ingreso_codigo_ingreso)
    REFERENCES Ingreso (codigo_ingreso);

-- Reference: Medico_Ingreso_Medico (table: Medico_Ingreso)
ALTER TABLE Medico_Ingreso ADD CONSTRAINT Medico_Ingreso_Medico FOREIGN KEY Medico_Ingreso_Medico (Medico_codigo_medico)
    REFERENCES Medico (codigo_medico);

-- End of file.

