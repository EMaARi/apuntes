-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2021-02-10 09:25:16.82

-- tables
-- Table: Producto
CREATE TABLE Producto (
    codigo_producto char(10) NOT NULL,
    precio decimal(6,2) NOT NULL,
    existencias int NOT NULL,
    descripcion text NOT NULL,
    CONSTRAINT Producto_pk PRIMARY KEY (codigo_producto)
);

-- Table: Producto_Cliente
CREATE TABLE Producto_Cliente (
    codigo_compra char(10) NOT NULL,
    Producto_codigo_producto char(10) NOT NULL,
    cliente_codigo_cliente char(10) NOT NULL,
    CONSTRAINT Producto_Cliente_pk PRIMARY KEY (codigo_compra)
);

-- Table: Proveedor
CREATE TABLE Proveedor (
    codigo_proveedor char(10) NOT NULL,
    nombre varchar(5) NOT NULL,
    apellidos varchar(5) NOT NULL,
    direccion varchar(10) NOT NULL,
    tfno char(9) NOT NULL,
    CONSTRAINT Proveedor_pk PRIMARY KEY (codigo_proveedor)
);

-- Table: Proveerdor_Producto
CREATE TABLE Proveerdor_Producto (
    codigo_suministro char(10) NOT NULL,
    Producto_codigo_producto char(10) NOT NULL,
    Proveedor_codigo_proveedor char(10) NOT NULL,
    CONSTRAINT Proveerdor_Producto_pk PRIMARY KEY (codigo_suministro)
);

-- Table: cliente
CREATE TABLE cliente (
    codigo_cliente char(10) NOT NULL,
    nombre varchar(5) NOT NULL,
    apellidos varchar(5) NOT NULL,
    direccion varchar(10) NOT NULL,
    tfno char(9) NOT NULL,
    CONSTRAINT cliente_pk PRIMARY KEY (codigo_cliente)
);

-- foreign keys
-- Reference: Producto_Cliente_Producto (table: Producto_Cliente)
ALTER TABLE Producto_Cliente ADD CONSTRAINT Producto_Cliente_Producto FOREIGN KEY Producto_Cliente_Producto (Producto_codigo_producto)
    REFERENCES Producto (codigo_producto);

-- Reference: Producto_Cliente_cliente (table: Producto_Cliente)
ALTER TABLE Producto_Cliente ADD CONSTRAINT Producto_Cliente_cliente FOREIGN KEY Producto_Cliente_cliente (cliente_codigo_cliente)
    REFERENCES cliente (codigo_cliente);

-- Reference: Proveerdor_Producto_Producto (table: Proveerdor_Producto)
ALTER TABLE Proveerdor_Producto ADD CONSTRAINT Proveerdor_Producto_Producto FOREIGN KEY Proveerdor_Producto_Producto (Producto_codigo_producto)
    REFERENCES Producto (codigo_producto);

-- Reference: Proveerdor_Producto_Proveedor (table: Proveerdor_Producto)
ALTER TABLE Proveerdor_Producto ADD CONSTRAINT Proveerdor_Producto_Proveedor FOREIGN KEY Proveerdor_Producto_Proveedor (Proveedor_codigo_proveedor)
    REFERENCES Proveedor (codigo_proveedor);

-- End of file.

