-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2021-02-10 09:05:08.036

-- tables
-- Table: Alumno
CREATE TABLE Alumno (
    expendiente char(15) NOT NULL,
    nombre varchar(25) NOT NULL,
    apellidos varchar(40) NOT NULL,
    fecha_nac date NOT NULL,
    Delegado_codigo_delegado char(10) NOT NULL,
    CONSTRAINT Alumno_pk PRIMARY KEY (expendiente)
);

-- Table: Alumno_Modulo
CREATE TABLE Alumno_Modulo (
    codigo_curso char(10) NOT NULL,
    Modulo_codigo_modulo char(10) NOT NULL,
    Alumno_expendiente char(15) NOT NULL,
    CONSTRAINT Alumno_Modulo_pk PRIMARY KEY (codigo_curso)
);

-- Table: Delegado
CREATE TABLE Delegado (
    codigo_delegado char(10) NOT NULL,
    Alumno_expendiente char(15) NOT NULL,
    CONSTRAINT Delegado_pk PRIMARY KEY (codigo_delegado)
);

-- Table: Modulo
CREATE TABLE Modulo (
    codigo_modulo char(10) NOT NULL,
    nombre varchar(25) NOT NULL,
    CONSTRAINT Modulo_pk PRIMARY KEY (codigo_modulo)
);

-- Table: Profesor
CREATE TABLE Profesor (
    dni char(8) NOT NULL,
    nombre varchar(25) NOT NULL,
    direccion varchar(100) NOT NULL,
    tfno varchar(9) NOT NULL,
    CONSTRAINT Profesor_pk PRIMARY KEY (dni)
);

-- Table: Profesor_Modulo
CREATE TABLE Profesor_Modulo (
    codigo_imparte char(10) NOT NULL,
    Profesor_dni char(8) NOT NULL,
    Modulo_codigo_modulo char(10) NOT NULL,
    CONSTRAINT Profesor_Modulo_pk PRIMARY KEY (codigo_imparte)
);

-- foreign keys
-- Reference: Alumno_Delegado (table: Alumno)
ALTER TABLE Alumno ADD CONSTRAINT Alumno_Delegado FOREIGN KEY Alumno_Delegado (Delegado_codigo_delegado)
    REFERENCES Delegado (codigo_delegado);

-- Reference: Alumno_Modulo_Alumno (table: Alumno_Modulo)
ALTER TABLE Alumno_Modulo ADD CONSTRAINT Alumno_Modulo_Alumno FOREIGN KEY Alumno_Modulo_Alumno (Alumno_expendiente)
    REFERENCES Alumno (expendiente);

-- Reference: Alumno_Modulo_Modulo (table: Alumno_Modulo)
ALTER TABLE Alumno_Modulo ADD CONSTRAINT Alumno_Modulo_Modulo FOREIGN KEY Alumno_Modulo_Modulo (Modulo_codigo_modulo)
    REFERENCES Modulo (codigo_modulo);

-- Reference: Delegado_Alumno (table: Delegado)
ALTER TABLE Delegado ADD CONSTRAINT Delegado_Alumno FOREIGN KEY Delegado_Alumno (Alumno_expendiente)
    REFERENCES Alumno (expendiente);

-- Reference: Profesor_Modulo_Modulo (table: Profesor_Modulo)
ALTER TABLE Profesor_Modulo ADD CONSTRAINT Profesor_Modulo_Modulo FOREIGN KEY Profesor_Modulo_Modulo (Modulo_codigo_modulo)
    REFERENCES Modulo (codigo_modulo);

-- Reference: Profesor_Modulo_Profesor (table: Profesor_Modulo)
ALTER TABLE Profesor_Modulo ADD CONSTRAINT Profesor_Modulo_Profesor FOREIGN KEY Profesor_Modulo_Profesor (Profesor_dni)
    REFERENCES Profesor (dni);

-- End of file.

