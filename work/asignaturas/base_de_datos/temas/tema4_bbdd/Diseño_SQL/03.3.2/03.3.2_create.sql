-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2021-02-10 08:53:38.803

-- tables
-- Table: Camion
CREATE TABLE Camion (
    matricula char(10) NOT NULL,
    modelo varchar(25) NOT NULL,
    potencia varchar(10) NOT NULL,
    tipo varchar(20) NOT NULL,
    CONSTRAINT Camion_pk PRIMARY KEY (matricula)
);

-- Table: Camionero
CREATE TABLE Camionero (
    dni char(8) NOT NULL,
    nombre varchar(25) NOT NULL,
    poblacion varchar(15) NOT NULL,
    direccion varchar(100) NOT NULL,
    tfno char(9) NOT NULL,
    salario decimal(6,2) NOT NULL,
    CONSTRAINT Camionero_pk PRIMARY KEY (dni)
);

-- Table: Camionero_Camion
CREATE TABLE Camionero_Camion (
    codigo_movilidad char(10) NOT NULL,
    Camionero_dni char(8) NOT NULL,
    Camion_matricula char(10) NOT NULL,
    CONSTRAINT Camionero_Camion_pk PRIMARY KEY (codigo_movilidad)
);

-- Table: Camionero_Paquete
CREATE TABLE Camionero_Paquete (
    codigo_distribucion char(10) NOT NULL,
    Camionero_dni char(8) NOT NULL,
    Paquete_codigo_paquete char(10) NOT NULL,
    CONSTRAINT Camionero_Paquete_pk PRIMARY KEY (codigo_distribucion)
);

-- Table: Paquete
CREATE TABLE Paquete (
    codigo_paquete char(10) NOT NULL,
    direccion varchar(100) NOT NULL,
    destinatario varchar(100) NOT NULL,
    descripcion text NOT NULL,
    CONSTRAINT Paquete_pk PRIMARY KEY (codigo_paquete)
);

-- Table: Paquete_Provincia
CREATE TABLE Paquete_Provincia (
    codigo_destino char(10) NOT NULL,
    Paquete_codigo_paquete char(10) NOT NULL,
    Provincia_codigo_provincia char(10) NOT NULL,
    CONSTRAINT Paquete_Provincia_pk PRIMARY KEY (codigo_destino)
);

-- Table: Provincia
CREATE TABLE Provincia (
    codigo_provincia char(10) NOT NULL,
    nombre varchar(25) NOT NULL,
    CONSTRAINT Provincia_pk PRIMARY KEY (codigo_provincia)
);

-- foreign keys
-- Reference: Camionero_Camion_Camion (table: Camionero_Camion)
ALTER TABLE Camionero_Camion ADD CONSTRAINT Camionero_Camion_Camion FOREIGN KEY Camionero_Camion_Camion (Camion_matricula)
    REFERENCES Camion (matricula);

-- Reference: Camionero_Camion_Camionero (table: Camionero_Camion)
ALTER TABLE Camionero_Camion ADD CONSTRAINT Camionero_Camion_Camionero FOREIGN KEY Camionero_Camion_Camionero (Camionero_dni)
    REFERENCES Camionero (dni);

-- Reference: Camionero_Paquete_Camionero (table: Camionero_Paquete)
ALTER TABLE Camionero_Paquete ADD CONSTRAINT Camionero_Paquete_Camionero FOREIGN KEY Camionero_Paquete_Camionero (Camionero_dni)
    REFERENCES Camionero (dni);

-- Reference: Camionero_Paquete_Paquete (table: Camionero_Paquete)
ALTER TABLE Camionero_Paquete ADD CONSTRAINT Camionero_Paquete_Paquete FOREIGN KEY Camionero_Paquete_Paquete (Paquete_codigo_paquete)
    REFERENCES Paquete (codigo_paquete);

-- Reference: Paquete_Provincia_Paquete (table: Paquete_Provincia)
ALTER TABLE Paquete_Provincia ADD CONSTRAINT Paquete_Provincia_Paquete FOREIGN KEY Paquete_Provincia_Paquete (Paquete_codigo_paquete)
    REFERENCES Paquete (codigo_paquete);

-- Reference: Paquete_Provincia_Provincia (table: Paquete_Provincia)
ALTER TABLE Paquete_Provincia ADD CONSTRAINT Paquete_Provincia_Provincia FOREIGN KEY Paquete_Provincia_Provincia (Provincia_codigo_provincia)
    REFERENCES Provincia (codigo_provincia);

-- End of file.

