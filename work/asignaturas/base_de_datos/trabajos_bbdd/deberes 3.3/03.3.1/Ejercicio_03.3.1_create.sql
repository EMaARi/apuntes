-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2021-02-10 08:36:00.558

-- tables
-- Table: Cliente
CREATE TABLE Cliente (
    dni char(8) NOT NULL,
    nombre varchar(25) NOT NULL,
    apellidos varchar(40) NOT NULL,
    fecha_nac date NOT NULL,
    tfno char(9) NOT NULL,
    CONSTRAINT Cliente_pk PRIMARY KEY (dni)
);

-- Table: Cliente_Producto
CREATE TABLE Cliente_Producto (
    codigo_compra char(10) NOT NULL,
    Cliente_dni char(8) NOT NULL,
    Producto_codigo char(10) NOT NULL,
    CONSTRAINT Cliente_Producto_pk PRIMARY KEY (codigo_compra)
);

-- Table: Producto
CREATE TABLE Producto (
    codigo char(10) NOT NULL,
    nombre varchar(20) NOT NULL,
    precio decimal(6,2) NOT NULL,
    CONSTRAINT Producto_pk PRIMARY KEY (codigo)
);

-- Table: Producto_Proveedor
CREATE TABLE Producto_Proveedor (
    codigo_suministro char(10) NOT NULL,
    Proveedor_nif char(8) NOT NULL,
    Producto_codigo char(10) NOT NULL,
    CONSTRAINT Producto_Proveedor_pk PRIMARY KEY (codigo_suministro)
);

-- Table: Proveedor
CREATE TABLE Proveedor (
    nif char(8) NOT NULL,
    nombre varchar(25) NOT NULL,
    dierccion varchar(255) NOT NULL,
    CONSTRAINT Proveedor_pk PRIMARY KEY (nif)
) COMMENT '
';

-- foreign keys
-- Reference: Table_4_Cliente (table: Cliente_Producto)
ALTER TABLE Cliente_Producto ADD CONSTRAINT Table_4_Cliente FOREIGN KEY Table_4_Cliente (Cliente_dni)
    REFERENCES Cliente (dni);

-- Reference: Table_4_Producto (table: Cliente_Producto)
ALTER TABLE Cliente_Producto ADD CONSTRAINT Table_4_Producto FOREIGN KEY Table_4_Producto (Producto_codigo)
    REFERENCES Producto (codigo);

-- Reference: Table_5_Producto (table: Producto_Proveedor)
ALTER TABLE Producto_Proveedor ADD CONSTRAINT Table_5_Producto FOREIGN KEY Table_5_Producto (Producto_codigo)
    REFERENCES Producto (codigo);

-- Reference: Table_5_Proveedor (table: Producto_Proveedor)
ALTER TABLE Producto_Proveedor ADD CONSTRAINT Table_5_Proveedor FOREIGN KEY Table_5_Proveedor (Proveedor_nif)
    REFERENCES Proveedor (nif);

-- End of file.

