-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2021-02-10 09:12:24.084

-- tables
-- Table: Cliente
CREATE TABLE Cliente (
    nif char(8) NOT NULL,
    nombre varchar(10) NOT NULL,
    ciudad varchar(10) NOT NULL,
    direccion varchar(30) NOT NULL,
    tfno char(9) NOT NULL,
    CONSTRAINT Cliente_pk PRIMARY KEY (nif)
);

-- Table: Cliente_Coche
CREATE TABLE Cliente_Coche (
    codigo_compra char(10) NOT NULL,
    Cliente_nif char(8) NOT NULL,
    Coche_matricula char(15) NOT NULL,
    CONSTRAINT Cliente_Coche_pk PRIMARY KEY (codigo_compra)
);

-- Table: Coche
CREATE TABLE Coche (
    matricula char(15) NOT NULL,
    marca varchar(10) NOT NULL,
    modelo varchar(10) NOT NULL,
    color varchar(5) NOT NULL,
    precio decimal(8,2) NOT NULL,
    CONSTRAINT Coche_pk PRIMARY KEY (matricula)
);

-- Table: Coche_Revision
CREATE TABLE Coche_Revision (
    codigo_registro char(15) NOT NULL,
    Coche_matricula char(15) NOT NULL,
    Revision_codigo_revision char(10) NOT NULL,
    CONSTRAINT Coche_Revision_pk PRIMARY KEY (codigo_registro)
);

-- Table: Revision
CREATE TABLE Revision (
    codigo_revision char(10) NOT NULL,
    filtro varchar(5) NOT NULL,
    aceite varchar(5) NOT NULL,
    frenos varchar(5) NOT NULL,
    CONSTRAINT Revision_pk PRIMARY KEY (codigo_revision)
);

-- foreign keys
-- Reference: Cliente_Coche_Cliente (table: Cliente_Coche)
ALTER TABLE Cliente_Coche ADD CONSTRAINT Cliente_Coche_Cliente FOREIGN KEY Cliente_Coche_Cliente (Cliente_nif)
    REFERENCES Cliente (nif);

-- Reference: Cliente_Coche_Coche (table: Cliente_Coche)
ALTER TABLE Cliente_Coche ADD CONSTRAINT Cliente_Coche_Coche FOREIGN KEY Cliente_Coche_Coche (Coche_matricula)
    REFERENCES Coche (matricula);

-- Reference: Coche_Revision_Coche (table: Coche_Revision)
ALTER TABLE Coche_Revision ADD CONSTRAINT Coche_Revision_Coche FOREIGN KEY Coche_Revision_Coche (Coche_matricula)
    REFERENCES Coche (matricula);

-- Reference: Coche_Revision_Revision (table: Coche_Revision)
ALTER TABLE Coche_Revision ADD CONSTRAINT Coche_Revision_Revision FOREIGN KEY Coche_Revision_Revision (Revision_codigo_revision)
    REFERENCES Revision (codigo_revision);

-- End of file.

