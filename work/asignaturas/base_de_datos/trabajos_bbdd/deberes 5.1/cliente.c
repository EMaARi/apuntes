
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main() {
    FILE* fichero;
    int i=0;
    // Tabla cliente consta de id_cliente, nombre, ciudad, categoria
    char nombre[][50]={ "Luis", "Maria", "Mercedes", "Ana", "Teo", "Lorena", "Pablo", "Marcos", "Miguel", "Santiago"};
    char ciudad [][50]={"Madrid", "Barcelona", "Toledo", "Valladolid", "Sevilla"};
    
    int categoria [][2]={1,2,3};
    char sentencia[100]="";
    int numero;
    char id[2];
    char comillas[2]="\'";
    
    fichero = fopen("Cliente.sql", "wt");
    
    for (i=0;i<10;i++){
         strcat(sentencia,"INSERT INTO mydb.cliente VALUES (");
         sprintf(id, "%d", i);
         strcat(sentencia,id);
         strcat(sentencia,",");
         numero = rand() % 11;
         strcat(sentencia,comillas);
         strcat(sentencia,nombre[numero]);
         strcat(sentencia,comillas);
         strcat(sentencia,",");
         numero = rand() % 5;
        strcat(sentencia,comillas);
         strcat(sentencia,ciudad[numero]);
         strcat(sentencia,comillas);
         //strcat(sentencia,",");
         //numero = rand() % 3; 
        // numero2 = (char)numero; 
         //strcat(sentencia,categoria[numero]);
         strcat(sentencia,");\n");
         printf("%s",sentencia);
         fputs(sentencia,fichero);
         *sentencia = NULL;
    }
    fclose(fichero);
    printf("Proceso completado");
    return 0;
}