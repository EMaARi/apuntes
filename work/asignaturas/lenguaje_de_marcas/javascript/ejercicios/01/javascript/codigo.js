
function mensaje(){
    alert("Hola mundo!");
    /*Este mensaje funaciona con alert("<mensaje>");*/
    alert("Soy el primer script");
}

var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

var valores = [true, 5, false, "hola", "adios", 2];

mensaje();

for (let i = 0; i < 12; i++){
    alert("Mes " + (i+1) + " " + meses[i]);
}
