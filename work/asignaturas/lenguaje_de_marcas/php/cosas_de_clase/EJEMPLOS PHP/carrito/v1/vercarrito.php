<?php
//Creamos sesión y una conexión con la base de datos.
ob_start("ob_gzhandler");
session_start();
extract($_REQUEST);

$servidor = "localhost";
$BD = "prueba";
$nombre = "usuario";
$contraseña = "1234";

// Create connection
$conexion = mysqli_connect($servidor, $nombre, $contraseña, $BD);
// Comprobar la conexión
if (!$conexion) {
    die("Ha fallado la conexión: " . mysqli_connect_error());
}


//Comprobamos que la variable $carro tenga valor.
if(isset($_SESSION['carro'])){
  $carro=$_SESSION['carro'];
}
else {
  $carro=false;
}

//Hacemos la consulta a la base de datos para mostrar los productos
$consulta=("SELECT * FROM productos WHERE idProductos=".$id.";");
$resultado = mysqli_query($conexion, $consulta);

?>

<!DOCTYPE html PUBLIC>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
     <link rel="stylesheet" href="estilo_cajas.css">
    <title>Cesta</title>
  </head>
  <body>

 <?php if($carro){ // Si $carro tiene algo lo muestra en la tabla. ?>

<table>
    <tr>
      <td width="9%">CANTIDAD</td>
      <td width="14%">IMAGEN</td>
      <td width="48%">NOMBRE</td>
      <td width="19%">PRECIO</td>
      <td width="10%">BORRAR</td>
    </tr>

    <?php 

      $color=array("#666666","#777777");
      $contador=0;
      $suma=0;

      foreach($carro as $k => $v){
         $row = mysqli_fetch_assoc($resultado);
         $subto=$v['cantidad']*$v['Precio'];
         $suma=$suma+$subto;
         $contador++; 
      ?>

<form name="a<?php echo $v['identificador'] ?>" method="post" action="
agregacar.php?<?php echo SID ?>&id=<?php echo $row['id']?>"
id="a<?php echo $v['identificador'] ?>">
      <tr bgcolor="<?php echo $color[$contador%2]; ?>">
        <td>
          <?php 
          echo $v['cantidad'];  ?>
        </td>
        <td>
            <?php //ponemos la miniatura del producto con link a su detalle. ?>
            <!--<img src="imagen.png" width=10%; height=10%/></a>-->
        </td>
        <td>
            <?php echo $v['Nombre'] ?></a>
        </td>
        <td>
            <?php echo $v['Precio'] ?> €<br />
        </td>
        <td>
              <a href="borracar.php?<?php echo SID ?>&id=<?php echo $v['id'] ?>&precio=<?php echo $precio ?>"><img src="img/btBorrar.gif" alt="Borrar
             articulo" width="20" height="20" /></a>
        </td>
     </tr>
   </form>
     <tr>
<?php } //fin foreach ?>
      <td colspan="5"><p>TOTAL  PRODUCTOS:
      <?php echo count($carro); ?><br />
      TOTAL PRECIO: <?php echo number_format($suma,2); ?> &euro;</p></td>
    </tr>
</table>
<div id="boton"><a href="catalogo.php?<?php echo SID?>">CONTINUAR COMPRA</a></div>
<div id="boton"><a href="pedido.php?<?php echo SID?>&precio=<?php echo $precio ?>">FINALIZAR COMPRA</a></div>
<?php }else{ // // Si $carro NO tiene nada solo muestra un link a index.php.?>
<p>El carrito de compra está vacío.</p>
<div id="boton"><a href="catalogo.php<?php echo SID?>">CONTINUAR COMPRA</a></div>
<?php }?>


<?php //Al final del archivo liberamos recursos

ob_end_flush();
mysqli_free_result($resultado);
mysqli_close($conexion); 

?>
</body>
</html>
