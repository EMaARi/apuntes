
//Este es un comentrario de una linea
/*Comentario de varias lineas*/

int a;        //Variable global.
              //Declara fuera de todas las funciones.
              //Se puede usar desde esta linea en adelante.
              //No se deben usar de manera habitual.
              //Variable globlal por defecto = 0

void          //No devuelve nada: void = vacio
haz_algo      //Usamos la notacion underscore
              //En camelcase HazAlgo (no usar)
{
    return;   //Cuando se ejecuta vuelve al lugar desde donde le han llamado.
              //Puede de volver un valor o no.
              //Es inecesario en las funciones void.
              //Cuando una funcion void llega al final ejecuta un retur por si solo.
}

int           //main devuelve un entero.
main ()       //Declarar la funcion main.
{
    int c;    //Variable local.
              //Se definen dentro de {}
              //Solo se conocen en la funcion.
              //Al terminar la funcion su valor desaparece.
              //Por defecto ? (rubbish)


    2 + a;    //Las instrucciones terminan en ;
              //Esto es una expresion.
              //Todo tien un Valor de Retorno, pero...
              //¿Qué estamos haciendo con la Variable de retorno?

    return 0; //Devuelve un valor a la funcion que llamo.
              //main => bash
              //0 => todo ok
              //!= 0 => wrong
}
