
#include <stdio.h>
#include <stdlib.h>

#define NUM     4

/* Stringnify */
#define ELVALOR(x) printf ("El valor de " #x " es: %i\n", x);

#define NUEVAVAR(x) char variable_##x[] = #x;

int main(int argc, int *argv[]){

  int a = NUM;
  //Objetivo:
  //char variable_a[] = 'a';

  NUEVAVAR(a);

  ELVALOR(a);
  ELVALOR(NUM);

  printf ("%s\n, variable_a");

    return EXIT_SUCCESS;
}

