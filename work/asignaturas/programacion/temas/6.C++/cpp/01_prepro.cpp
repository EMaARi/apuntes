#include <stdio.h>
#include <stdlib.h>

/*El preprocesador */

#define EURO 166.386
#define PTAS 1 / EURO /* Se puede hacer usos de sustituciones previas */

//No se puede hacer:
//#define EURO 166.386 //Lo que vale 1 euro
//
//Sí se puede hacer:
//#define EURO 166.386 /* Lo que vale 1 euro*/

/*MACROS*/

#define IMPRIME           printf /* Sustituimos las constantes por código */
#define PON_ENTERO(x)     printf ("%i\n", (x));
#define SUMA2(x)           x + 2
/* 0 2 paráemetros (o más) */
#define SUMA(x,y)         x + y /* Macro mal hecha */
#define SUMB(x,y)         ((x) + (y)) /* Macro bien hecha */

/* 0 un número variable de párametros */
/* MACROS variadicas */
#define ESCRIBE(...) prinf (__VA_ARGS__)

/**********************/
/*    COSAS UTILES    */
/**********************/

// Sí entre dos coadenasw de caracteres solo median
// whitespaces (\n, \t, ' ') se consedera
// la misma cadena de caracteres

// Hay que poner todos los parametros entre paréntesis
// y el resultado global de la macro entre parentesis

// Son utiles para poner textos grandes
#define LINEA "comando argumento1 argumento2"
#define AYUDA "             \n\
    01_prepro <argumentos>  \n\
    "

int main (int argc, char *argv[]){

    IMPRIME ("El precio del euro en persetas es: %lf\n", EURO);
    PON_ENTERO(2);
    PON_ENTERO(3);
    PON_ENTERO(SUMA2(5));  // El parámetro de un PON_ENTERO es: 5 + 2 
    PON_ENTERO(SUMA(2, 4));

    ESCRIBE ("$i\n", 2);

    /* La macro se expande así:               */
    /* prinf ("Macro Variadica: " "%i\n", 2); */

    PON_ENTERO(SUMA(2, 4) * 6);
    /* Expansión */

    PON_ENTERO(SUMB(2, 4) * 6);

    IMPRIME (AYUDA);

    return EXIT_SUCCESS;
}
