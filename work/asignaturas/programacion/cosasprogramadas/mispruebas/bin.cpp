#include <stdio.h>
#include <stdlib.h>

void metodo1(unsigned decimal){
    while (decimal > 0) {
        printf ("%u", decimal % 2);
        decimal >>= 1;
    }
    printf ("\n");
}

void metodo2(unsigned decimal){
    for ( ;decimal > 0; decimal >>= 1)
        printf ("%u", decimal % 2);

      printf ("\n");
}


int main(int argc, char *argv[]){

    unsigned decimal;

    scanf (" %u", &decimal);
    printf ("%u\n", decimal);

    /*
            /=2 %
    14 => 1110
     7 => 0111 (0)
     3 => 0011 (10)
     1 => 0001 (110)
     0 => 0000 (1110)
     */
    metodo2 (decimal);


    return EXIT_SUCCESS;
}

