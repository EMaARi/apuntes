#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>


#define MAX 0x20


int main(){
    unsigned rep=0;
    char nombre [MAX];

    do {
        printf ("Repeticiones: ");
        __fpurge (stdin);
    } while ( !scanf (" %u", &rep) );

    printf ("Introduce tu nombre: ");
    scanf (" %s", nombre);

    for (unsigned i=0; i<=rep; i++)
        printf ("(%u/%u) %s\n", i, rep, nombre);


    return EXIT_SUCCESS;
}

