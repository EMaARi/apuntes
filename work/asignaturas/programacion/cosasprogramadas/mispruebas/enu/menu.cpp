#include <stdio.h>
#include <stdlib.h>

#define MENU "                     \n\
        ~~~~~~~~~~~~~~             \n\
        |    Menu    |             \n\
        ~~~~~~~~~~~~~~             \n\
                                   \n\
  Opciones:                        \n\
  --------                         \n\
      1. Suma                      \n\
      2. Resta                     \n\
      3. Multiplicación            \n\
      4. División                  \n\
      5. Salir                     \n\
                                   \n\
                                   \n\
        Tu opción:                 \n\"

int main(int argc, int *argv[]){

    printf (MENU);

    return EXIT_SUCCESS;
}
