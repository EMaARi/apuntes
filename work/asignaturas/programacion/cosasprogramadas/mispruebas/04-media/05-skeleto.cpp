#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>


void leer (double *var1, double *var2, double *var3) {
    printf ("Introduce valores a x,y,z: ");
    scanf (" %*1[[{] %lf %*1[,-/] %lf %*1[,-/] %lf %*1[]}] ", var1, var2, var3);
}

void imprimir (double x, double y, double z) {
    printf ("[ %.2lf / %.2lf / %.2lf ] \n", x, y, z);
}

int main(int argc, char *argv[]){
    double x, y, z;

    leer (&x, &y, &z);
    imprimir (x, y, z);

    return EXIT_SUCCESS;
}
