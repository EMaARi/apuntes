#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>


int main(int argc, char *argv[]){
    unsigned rep;
    int i;
    double num, suma, div;

    printf ("Introduce numero de repeticiones: ");
    scanf (" %u", &rep);

    for ( i=1 ; i<=rep ; i++ ) {
        printf ("(%i/%u) Introduce numero: ", i, rep);
        scanf (" %lf", &num);
        printf ("(%i/%u) = %.2lf \n", i, rep, num);
        suma = suma + num;
    }

    div = suma/rep;
    printf ("Suma total de numeros: (%.2lf)\n", suma);
    printf ("Media de los numeros: (%.5lf)\n", div);

    return EXIT_SUCCESS;
}
