#include <stdio.h>
#include <stdlib.h>

#define N 100

int le_da_la_gana_al_usuario (int s, int *room){
    return room[s - 1] < 0 ? 0 : 1;
}

int main(int argv, char *argc[]){

    int *room;
    int summit = 0;

    room = (int *) malloc (sizeof (int));

    do{
        printf ("Número: ");
        scanf ("%i", &room[summit++]);
        room = (int *) realloc (room, (summit +1) * sizeof (int));
      } while (le_da_la_gana_al_usuario (summit, room));
    summit--;

    printf ("\n");
    for (int i=0; i<summit; i++)
        printf("%i: %i\n", i+1, room[i]);
    printf ("\n");

    free (room);

    return EXIT_SUCCESS;
}
