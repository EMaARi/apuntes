#include <stdio.h>
#include <stdlib.h>

int main(int argv, char *argc[]){

    int num,
        suma=1;

    printf("Introduce numero para saber factorial: ");
    scanf("%i", &num);

    for(int i=num; i>=0; i--){
        printf("%i.%i Total=%i\n", i, num, suma);
        i;
        num--;
    }
    printf("Suma total=%i\n", suma);

    return EXIT_SUCCESS;
}
