#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


#define N 3

int mira(int A[N][N], int f, int c){
    return A[f % N][c % N];
}

int main(int argc, char *argv[]){

    int fila, cols;
    int A[N][N] = { { 1, 2,  3},
                    {-6, 9, 12},
                    { 3, 8, -1} };


    printf("Introduce fila: ");
    scanf("%i", &fila);
    printf("Introduce columna: ");
    scanf("%i", &cols);

    printf(" %3i\n", A[fila][cols]);

    return EXIT_SUCCESS;
}
