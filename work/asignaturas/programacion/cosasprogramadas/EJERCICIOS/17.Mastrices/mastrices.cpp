#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


#define N 3

int mira(int A[N][N], int f, int c){
    return A[f % N][c % N];
}

int main(int argc, char *argv[]){

    int num,
        par = 0,
        sum = 0;
    int A[N][N] = { { 1, 2,  3},
                    {-6, 9, 12},
                    { 3, 8, -1} };

    for(int x=0; x<N; x++){
        for(int i=x; i<N; i++){
            num = num * A[i][i];
            printf(" %3i\n", num);
        }
        sum += num;
        printf(" %3i\n", sum);
    }
        printf("numero final [%3i]\n", num);

    return EXIT_SUCCESS;
}
