#include <stdio.h>
#include <stdlib.h>


#define MAX 60

void introducir(char *nom, int edad){
    printf("Introduce tu nombre: ");
    scanf(" %s", nom);

    printf("\n");

    printf("Introduce tu edad: ");
    scanf("%i", &edad);
    printf("%i", edad);

    printf("\n");

}


int main (int argc, char *argv[]){

    char nom[MAX];
    int edad;

    introducir(nom, edad);
    printf("%i", edad);
    printf("Tu nombre es: %s\t edad:(%i)\n", nom, edad);

    return EXIT_SUCCESS;
}
