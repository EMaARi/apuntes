#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]){

    int num,
        lim,
        suma = 0;

    printf("Introduce limite: ");
    scanf(" %i", &lim);

    for(int i=0; i<lim; i++){
      num = i*2;
      suma += num;
      printf("%5i", suma);
    }
      printf("\n");

    return EXIT_SUCCESS;
}
