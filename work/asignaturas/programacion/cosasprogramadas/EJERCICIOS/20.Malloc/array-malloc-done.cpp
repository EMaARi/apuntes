#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_W 5

int main (int argc, char *argv[]){

    char *palabra[MAX_W];

    for(int i=0; i<MAX_W; i++){
        printf("Introduce: ");
        scanf("%ms", &palabra[i]);
    }

    for(int i=0; i<MAX_W; i++){
        printf("%i.- %s\n", i+1, palabra[i]);
    }

    for(int i=0; i<MAX_W; i++){
        free (palabra[i]);
    }

    return EXIT_SUCCESS;
}
