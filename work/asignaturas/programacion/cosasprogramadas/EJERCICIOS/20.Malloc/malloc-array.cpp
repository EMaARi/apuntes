#include <stdio.h>
#include <stdlib.h>

#define N 5

int main (int argc, char *argv[]){

    int *p;

    p = (int *) malloc ( N * sizeof (int));

    for (int a=0; a<N; a++)
        p[a] = a + 1;

    p[N-1] = 0;

    for (int *a=p; *a!=0; a++)
        printf("%i ", *a);
    printf("\n");

    free (p);

    return EXIT_SUCCESS;
}
