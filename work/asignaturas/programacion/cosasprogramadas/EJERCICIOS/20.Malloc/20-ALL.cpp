#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char *argv[]){

    char **palabras;
    int num;

    printf("Introduce numero de palabras: ");
    scanf("%i", &num);

    palabras = (char **) malloc (num * sizeof (char *));
    bzero ((void *) palabras, sizeof (palabras));

    for(int i=0; i<num; i++){
        printf("Introduce: ");
        scanf("%ms", &palabras[i]);
    }

    for(int i=0; i<num; i++){
        printf("%i.- %s\n", i+1, palabras[i]);
    }

    for(int i=0; i<num; i++){
        free (palabras[i]);
    }

    return EXIT_SUCCESS;
}
