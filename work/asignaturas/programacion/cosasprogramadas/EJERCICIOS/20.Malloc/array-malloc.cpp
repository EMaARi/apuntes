#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 100

int main (int argc, char *argv[]){

    char nombre[MAX];

    printf("Introduce tu nombre: ");
    scanf("%s", nombre);

    int length = strlen (nombre);
    length++;

    char *palabra = (char *) malloc (length);
    strncpy (palabra, nombre, MAX);

    free (palabra);

    printf("Tu nombre %s", nombre);
        printf("\n");

    return EXIT_SUCCESS;
}
