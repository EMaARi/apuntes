#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
    int *p;
    p = (int *) malloc (sizeof (int));

    *p = 7;

    printf("&p: %p\n", &p);
    printf(" p: %p\n", p);
    printf("*p: %i\n", *p);
    printf("\n");

    free (p);

    return EXIT_SUCCESS;
}
