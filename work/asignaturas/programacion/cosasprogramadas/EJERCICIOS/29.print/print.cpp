#include <stdlib.h>
#include <stdio.h>
#include <strings.h>


int main(int argc, char *argv[]){

    FILE *pf;
    char all;

    pf = fopen ("text.txt", "r");
    if (!pf) {
        fprintf (stderr, "File not found.\n");
        return EXIT_FAILURE;
    }

    do {
        fscanf(pf, "%c", &all);
        printf("%c", all);
    } while (!feof(pf));

    fclose (pf);

    return EXIT_SUCCESS;
}
