#include <stdio.h>
#include <stdlib.h>

void incrementa_count (unsigned *c0, unsigned *c4, unsigned *c5, unsigned num){
        switch (num){
            case 0:
                (*c0)++;
                break;
            case 4:
                (*c4)++;
                break;
            case 5:
                (*c5)++;
                break;
        }
}

int main(int argc, char *argv[]){

    unsigned cantidad,
             cache,
             c0 = 0,
             c4 = 0,
             c5 = 0;

    printf("Introduce cantidad numeros: ");
    scanf(" %u", &cantidad);

    for(unsigned i=0; i<cantidad; i++){
        printf("Número: ");
        scanf (" %u", &cache);
        incrementa_count(&c0, &c4, &c5, cache);
    }


    return EXIT_SUCCESS;
}
