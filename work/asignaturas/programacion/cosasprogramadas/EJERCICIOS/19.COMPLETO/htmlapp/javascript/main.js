var WIDTH
var HEIGHT
var XC
var YC

var R = 200

var vertex = [
    [ R*.866,  R/2],
    [ 0     ,  R],
    [-R*.866,  R/2],
    [-R*.866, -R/2],
    [ 0     , -R],
    [ R*.866, -R/2]
]

function init () {
    WIDTH  = parseInt (canvas.dataset.width)
    HEIGHT = parseInt (canvas.dataset.height)
    XC = WIDTH  / 2
    YC = HEIGHT / 2
}

function x (xl) { return XC + xl }
function y (yl) { return YC - yl }

function draw (ctx, vertex) {
    ctx.moveTo (x (vertex[0][0]), y (vertex[0][1]))
    for (var i=1; i<vertex.length; i++)
        ctx.lineTo (x (vertex[i][0]), y (vertex[i][1]))
    ctx.lineTo (x (vertex[0][0]), y (vertex[0][1]))

    ctx.stroke ()
}

function main () {
    var canvas = document.getElementById("canvas")
    var ctx    = canvas.getContext("2d")
    var valor  = document.getElementById("angle").value
    var output  = document.getElementById("valor")

    valor.oninput = function() {
        
    }

    init ()
    draw (ctx, vertex)

    let angle = valor * Math.PI / 180

    var R = [

        [Math.cos (angle), -Math.sin (angle)],
        [Math.sin (angle),  Math.cos (angle)]
    ]

    var rotated = []
    for (var i=0; i<vertex.length; i++)
        rotated[i] = [
            vertex[i][0] * R[0][0] + vertex[i][1] * R[0][1],
            vertex[i][0] * R[1][0] + vertex[i][1] * R[1][1]
        ]

    draw (ctx, rotated)

}
