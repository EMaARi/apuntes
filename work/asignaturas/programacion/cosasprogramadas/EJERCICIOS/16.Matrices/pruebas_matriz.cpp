
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


int main(int argc, char *argv[]){

    int N_FILAS, K_MAX, N_COLUMN;

    int A[N_FILAS][K_MAX],
        B[K_MAX][N_COLUMN],
        C[N_FILAS][N_COLUMN];


    printf("Introduce el numero de FILAS: ");
    scanf("%i", &N_FILAS);
    printf("\n");

    printf("Introduce el numero de COLUMNAS/FILAS: ");
    scanf("%i", &K_MAX);
    printf("\n");

    printf("Introduce el numero de COLUMNAS: ");
    scanf("%i", &N_COLUMN);
    printf("\n");

    for (int l=0; l<N_FILAS; l++){
        for (int c=0; c<K_MAX; c++){
                printf("A [%i]/[%i]:", l, c);
            scanf("%i", &A[l][c]);
        }
        printf("\n");
    }
    for (int l=0; l<K_MAX; l++){
        for (int c=0; c<N_COLUMN; c++){
                printf("B [%i]/[%i]:", l, c);
            scanf("%i", &B[l][c]);
        }
        printf("\n");
    }


    for (int l=0; l<N_FILAS; l++){
        for (int c=0; c<K_MAX; c++)
            printf("%7i", A[N_FILAS][K_MAX]);
        printf("\n");
        }


    bzero(C, sizeof(C));

    /*
     bzero es para limpiar el contenido de C
     */

    for (int l=0; l<N_FILAS; l++)
        for (int c=0; c<N_COLUMN; c++)
            for (int k=0; k<K_MAX; k++)
                C[l][c] += A[l][k] * B[k][c];

    for (int l=0; l<N_FILAS; l++){
        for (int c=0; c<N_COLUMN; c++)
            printf("%i", C[N_FILAS][N_COLUMN]);
        printf("\n");
        }

    return EXIT_SUCCESS;
}
