#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


#define N 3

int main(int argc, char *argv[]){

    unsigned N_FILAS, N_COLUMN;
    int A[N][N] = { {1, 2, 3}, {-6, 9, 12}, {3, 8, -1} },
        B[N][N] = { {5, 3, 4}, {-4, 12, 1}, {1, 3,  2} },
        C[N][N];

    bzero(C, sizeof(C));

    /*
     bzero es para limpiar el contenido de C
     */

    for (int l=0; l<N; l++)
        for (int c=0; c<N; c++)
            for (int k=0; k<N; k++)
                C[l][c] += A[l][k] * B[k][c];

    for (int l=0; l<N; l++){
        for (int c=0; c<N; c++)
            printf("%7i", C[l][c]);
        printf("\n");
        }

    return EXIT_SUCCESS;
}
