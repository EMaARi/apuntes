#include <stdio.h>
#include <stdlib.h>


int main (int argc, char *argv[]){

    unsigned v0[10], v1[10];

    for(int i=0; i<10; i++)
        v0[i] = rand() % 10 + 1;
    for(int i=0; i<10; i++)
        v1[i] = rand() % 2 + 9;

    printf("-");
    for(int i=0; i<10; i++){
        printf("|%i", v0[i]);
    }
    printf("|-");
    for(int i=0; i<10; i++){
        printf("|%i", v0[i]);
    }
    printf("|-\n");

    for(int i=0; i<10; i++){
        printf("|%i-%i", v0[i], v1[i]);
    }
    printf("|-\n");

    return EXIT_SUCCESS;
}
