#include <stdio.h>
#include <stdlib.h>

#define N 100

int le_da_la_gana_al_usuario (int s, char *room){
    return room[s - 1] < 0 ? 0 : 1;
}

int main(int argv, char *argc[]){

    char **room;
    int summit = 0;

    room = (char *) malloc (sizeof (char));

    do{
        printf ("Nombre: ");
        scanf ("%s", &room[summit++]);
        room = (char *) realloc (room, (summit +1) * sizeof (char));
      } while (le_da_la_gana_al_usuario (summit, room));
    summit--;

    printf ("\n");
    for (int i=0; i<summit; i++)
        printf("%i: %s\n", i+1, room[i]);
    printf ("\n");

    free (room);

    return EXIT_SUCCESS;
}
