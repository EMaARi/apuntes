#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLIN 0x100

const char * const cprog = "cadenas";
char vprog[] = "programa";

void concatena (char *dest, const char *org, int max) {
    while (*dest != '\0') dest++;

    /* para meter un mensaje secreto */
    dest++;

    /* versiones alternativas
    while (*(dest++) != '\0');
    for (;*dest != '\0'; dest++);
    */
    for (int c=0; *org!='\0' && c<max; c++, ++org, ++dest)
        *dest = *org;
    *dest = *org;

    /* Versiones alternativas
    for (int c=0; *org!='\0' && c<max; c++, ++org)
        *(dest+c) = *org;

    for (int c=0; *org!='\0' && c<max; c++, ++org)
        dest[c] = *org;
    */
}

void cifra (char *frase, int clave) {
    while (*frase != '\0')
       *(frase++) += clave;
    /*
    for (; *frase != '\0'; frase++)
        *frase += clave;
    */
}

int main (int argc, char *argv[]) {

    char linea[MAXLIN];
    strcpy (linea, cprog); // Funcion insegura.
    strncpy (linea, cprog, MAXLIN);
    strcat (linea, " ");
    strncat (linea, vprog, MAXLIN - strlen(linea));

    concatena (linea, "Esto es un mensaje secreto", MAXLIN - strlen(linea));

    cifra (linea + strlen(linea) + 1, 3);

    printf ("%s\n", linea);
    printf ("Secreto: %s\n", linea + strlen(linea) + 1);

    cifra (linea + strlen(linea) + 1, -3);
    printf ("Secreto: %s\n", linea + strlen(linea) + 1);


    return EXIT_SUCCESS;
}
