#include <stdio.h>
#include <stdlib.h>

double suma (double *par) { return par[0] + par[1]; }
double rest (double *par) { return par[0] - par[1]; }
int ca2 (int op) { return ~op + 1; }

int main (int argc, char *argv[]) {

    double (*p[]) (void *) = {
        (double (*) (void *)) &suma,
        (double (*) (void *)) &rest,
        (double (*) (void *)) &ca2
    };
    int opcion;

    printf ("Suma o resta (0/1): ");
    scanf (" %i", &opcion);

    double *parametro = (double *) malloc (2 * sizeof (double));
    *parametro = 2;
    *(parametro + 1) = 3;
    double resultado = (*p[opcion]) ( (void *) parametro );

    printf ("2 %c 3 = %.2lf\n",
            opcion == 0 ? '+' : '-',
            resultado );    // 2000 (2,3)

    printf ("\n");

    return EXIT_SUCCESS;
}
