#include <stdio.h>
#include <stdlib.h>

int suma (int op1, int op2) { return op1 + op2; }
int rest (int op1, int op2) { return op1 - op2; }

int main (int argc, char *argv[]) {

    int (*p[2]) (int, int) = { &suma, &rest };
    int opcion;

    printf ("Suma o resta (0/1): ");
    scanf (" %i", &opcion);

    printf ("2 %c 3 = %i\n",
            opcion == 0 ? '+' : '-',
            (*p[opcion]) (2, 3) );    // 2000 (2,3)

    printf ("\n");

    return EXIT_SUCCESS;
}
