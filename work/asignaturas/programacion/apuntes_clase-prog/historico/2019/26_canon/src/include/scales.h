#ifndef __SCALES_H__
#define __SCALES_H__

enum TScMode {major, minor};
enum class TScName { Fa, Do, Sol, Re, La, Mi, Si, Solb, Reb, Lab, Mib, Sib };
enum class TNoteName {silence, Do, Re, Mi, Fa, Sol, La, Si};

struct TScale {
    enum TScMode mode;
    enum TScName nn_idx;     // Note name index;
};

extern const char     * const scales[2][12];
extern const char     * const note_names[8];
extern const char     * const outputnames[2][12][8];
extern const unsigned         name_len;

#ifdef __cplusplus
extern "C" {
#endif

    struct TScale create_scale ();
    struct TScale process_scale (char *txt);
    const char *get_scname (const struct TScale *sc);
    int note_value (struct TScale sc, enum TNoteName note);

#ifdef __cplusplus
}
#endif

#endif
