#include "score_parser.h"
#include "notes.h"
#include "scales.h"
#include "score.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_PARSE_BUFFER 0x100
// State Machine for reading score files.

unsigned active_duration;

void digest_notes (const char *notes, struct TScore *score) {

    /* Needed for string buffer operations */
    char note_input[5];
    int nread;              // Number of items read by scanf

    /* Actual note data */
    char note_name[NL];
    unsigned duration;

    struct TNote new_note;

    int pos = 0,
        leido = 0;
    while ( (nread = sscanf (&notes[pos], " %s%n", note_input, &leido)) > 0) {
        unsigned octave = 4;    // Default octave
        pos += leido;
        /* Get duration */
        nread = sscanf (note_input, "%i", &duration);
        if (!nread)
            duration = active_duration;

        /* note and octave */
        int i = 0;
        while (isdigit (note_input[i]))
            i++;
        sscanf (&note_input[i], "%s", note_name);
        int l = strlen (note_name);
        /* Separate octave from node name*/
        if (isdigit (note_name[l - 1] )) {
            octave += atoi( &note_name[l-1] ) - 1;
            note_name[l-1] = '\0';
        }
        new_note = create_note (duration, note_name, octave, score->scale);
        new_note.tone_id += octave * 12;
        push( &score->notes, new_note);
    }
}


void digest_line (const char *line, struct TScore *score) {
    char notes[MAX_PARSE_BUFFER];
    unsigned duration;

    // Read compass, duration and notes.
    sscanf ( line, "%*i : %u ( %[^)]  )", &duration, notes);
    active_duration = duration;
    digest_notes (notes, score);
}

void sanitize(char *buffer) {
    char *str = buffer;

    while(isspace((unsigned char)*str)) str++;
    strcpy (buffer, str);

    /* # for comments */
    while (*str != '#' && *str != '\n' && *str != '\0')
        str++;
    *str = '\0';
}

struct TScore *
parse_file (const char *filename) {
    FILE *pf = NULL;                  // Points to file in disk.
    char buffer[MAX_PARSE_BUFFER];
    char scale[MAX_PARSE_BUFFER];
    unsigned bpm;

    struct TScore *score = create_score ();

    /* Opening score file */
    if ( !(pf = fopen (filename, "r")) ) {
        fprintf (stderr, "File %s not found.\n", filename);
        exit (1);
    }

    /* Process score file*/
    /* Read bpm */
    fgets (buffer, MAX_PARSE_BUFFER, pf);
    sanitize (buffer);
    int nread = sscanf (buffer, "%[^:] : [%u]", scale, &bpm);
    if (nread) {
        score->scale = process_scale(scale);
        score->bpm = bpm;
    }
    /* Rest of line */
    fgets (buffer, MAX_PARSE_BUFFER, pf);

    /* Rest of score */
    while (fgets (buffer, MAX_PARSE_BUFFER, pf)) {
        sanitize (buffer);
        if (strcmp ("", buffer)) // Non empty line
            digest_line (buffer, score);
    }

    /* Close score file */
    fclose (pf);

    return score;
}

