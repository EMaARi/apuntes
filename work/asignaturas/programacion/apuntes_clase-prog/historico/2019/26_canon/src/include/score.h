#ifndef __SCORE_H__
#define __SCORE_H__

#include "scales.h"
#include "notes.h"
#include "stack.h"

/* Print dotted figures or decompose strategy */
#define NODOTS 1

/* Notes per line */
#define NPL 0x08

/* Default score BPM */
#define BPM 92



struct TScore {           // 4 4 score data
    unsigned bpm;         // beats per minute.
    struct TScale scale;  // Score scale
    struct TStack notes;  // Stack of notes.
};

#ifdef __cplusplus
extern "C" {
#endif

    void           print_sc      (const struct TScore *sc);
    struct TScore *create_score  ();
    void           destroy_score (struct TScore *sc);

#ifdef __cplusplus
}
#endif

#endif


