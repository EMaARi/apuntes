#include "usage.h"

#include <stdlib.h>

#define DFLT_NAME "videal"

const char *prog_name;
int inited = 0;

void usage_init (const char *name) {
    prog_name = name;
    inited = 1;
}

void print_usage (FILE *stream) {
    int exit_value = 0;


    if (!inited)
        prog_name = DFLT_NAME;

    if (stream == stderr)
        exit_value = 1;

    printf (
            "\n"
            "Usage: %s <score file>\n"
            "\n"
            "Converts a score into a sequence of video file names.\n"
            "\n"
            , prog_name
            );
    exit (exit_value);
}

