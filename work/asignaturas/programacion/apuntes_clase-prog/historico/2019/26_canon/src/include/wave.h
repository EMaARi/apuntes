#ifndef __WAVE_H__
#define __WAVE_H__

#include "notes.h"
#include "stack.h"
#include "score.h"

#include <stdint.h>

typedef uint64_t QWORD;
typedef uint32_t DWORD;
typedef uint16_t  WORD;
typedef uint8_t   BYTE;

/*general waveform format structure (information common to all formats) */
typedef struct waveformat_tag {
    WORD  wFormatTag;       /* format type */
    WORD  nChannels;        /* number of channels (i.e.mono,stereo...) */
    DWORD nSamplesPerSec;   /* samplerate */
    DWORD nAvgBytesPerSec;  /* for buffer estimation */
    QWORD  nBlockAlign;      /* blocksize of data */
} WAVEFORMAT;

typedef WAVEFORMAT      *PWAVEFORMAT;

struct TWav {
    WAVEFORMAT header;
    BYTE *data;
};

#ifdef __cplusplus
extern "C" {
#endif

    void write (const char *filename, struct TWav *score, unsigned freq);
    void play  (const struct TWav *sc, unsigned freq);

    struct TWav *render_score (const struct TScore *score, unsigned freq /* sampling freq*/);
    void destroy_wav (struct TWav *w);

#ifdef __cplusplus
}
#endif

#endif
