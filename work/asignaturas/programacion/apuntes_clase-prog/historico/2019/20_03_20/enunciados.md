# Enunciados para el día 20
(Y el fin de semana)

Hoy, y éste fin de semana vamos a dedicarlo a hacer un ejercicio de cierre,
donde damos por terminado el uso básico de bucles, arrays y funciones. Los
ejercicios realizados hasta hoy os han tenido que dejar un poso en el diseño
de algoritmos: que es lo que realmente es programar.

Vamos,pues, a programar el juego de la vida de Conwall.

Lee con atención el apartado _Juego_ de la wikipedia.

https://es.wikipedia.org/wiki/Juego_de_la_vida

Básicamente vas a tener que:

1. Definir una array bidimensional dinámicamente, usando las dimensiones que tenga el terminal en el momento de arrancar el programa. Para obtener las dimensiones del terminal, puedes inspirarte en el código que aparece en esta solución:  https://stackoverflow.com/questions/1022957/getting-terminal-width-in-c
1. Rellenar todo el array con 0's. Idealmente es mejor usar memset o bzero que usar un bucle.
1. Rellenar aleatoriamente el array usando rand() y srand(time(NULL) con 1's para decir que esa célula está viva. rand() % R nos va a dar un número entre 0 y R-1. En realidad no son números aleatorios. Sólo es una sucesión muy rara en la que parece que son aleatorios, pero se repite siempre igual. srand vale para decir por qué número empezamos. Viene de seed rand: darle una semilla a rand. time(NULL) nos da el número de segundos que han pasado desde el inicio de nuestra época (1 de enero de 1970).
1. Crear un segundo array de las dimensiones del primero para llevar el cómputo en cada celda de cuántos vecinos tiene vivos.
1. Repetir:
    1. Computar los vecinos que tiene cada celda (entre 0 y 8) en el array de cómputos.
    1. Regenerar el array de células.



Extra:

* ¿ Eres capaz de hacer una interfaz como la de entrada de datos de matrices que permita
inicialmente ir marcando que puntos van a estar seleccionados y cuales no ?
* ¿ Con qué tanto por ciento de población inicial tarda más el tablero en estabilizarse ?
* ¿ Eres capaz de gestionar en cada vuelta si ha cambiado la dimensión de la pantalla ?



