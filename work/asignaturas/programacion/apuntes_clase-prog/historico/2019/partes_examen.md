# Examen de la Evaluación 1

Evaluación 1: Fundamentos  

## Contenidos

Las siguientes contenidos ya se materializan fácticamente
en forma de pregunta en el examen.

### Parte I - Ejercicios
1. Sistemas Numéricos. Cambios de base, etc.
1. Operaciones aritmético-lógicas.
1. Teoría de la información.

### Parte II - BASH
1. Algo

### Parte II - Problemas
1. Estructura de un programa.
1. Manejo de los Operadores.
