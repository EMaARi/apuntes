#include <stdio.h>
#include <stdlib.h>

#define ERROR .00001

int factorial (int n) {
    if ( n == 0 )
        return 1;

    return n * factorial (n-1);
}

double e (unsigned n) {  /* e(n) contiene el valor de todos los términos mayores que n */
                         /* Orden de llamadas: e(0)->e(1)->e(2)->e(3)-> ... */
    double nuevo = 1. / factorial (n);
    if ( nuevo < ERROR ) /* El nuevo término ya es tan pequeño que aporta poco. */
        return nuevo;    /* No merece la pena calcular más términos. */
    return nuevo + e (n+1);
}

int main (int argc, char *argv[]) {

    printf ("e = %.5lf\n", e (0));  // Calculamos el valor desde el término 0.

    return EXIT_SUCCESS;
}
