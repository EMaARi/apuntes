# Enunciados 16

Imagináos que queremos calcular el área de una curva x^2 entre 0 y 4. Pues lo que vamos a hacer es coger los
puntos x=0,1,2,3 y ver cuánto vale la función ahí. Yo he obtenido:

```
(0, 0)
(1, 1)
(2, 4)
(3, 9)
```
Entonces, váis a pintar ahora en un papel una recta horizontal y váis a hacer marcas en el 0, 1, 2, 3.
Luego vamos a hacer rectángulos. En el 0 no hay que hacer nada, pues la altura es 0. Del 1 al 2 vamos a
pintar un rectángulo de altura 1. Del 2 al 3 uno de altura 4, y del 3 al 4 uno de altura 9.

Si uníis desde el 0 todas las esquinas superiores izquierdas de los rectángulos con una curva, váis a
obtener la función x^2, que tiene un área parecida a la suma de las áreas de los rectángulos.

Si quisiéramos una mejor aproximación habría que hacer rectángulos más estrechos.

Bien, para el ejemplo dado, el área sería algo así:

```math
A = 0 \times 1 + 1 \times 1 + 4 \times 1 + 9 \times 1
```

Si la anchura fuera la mitad, observa que habría el doble de puntos y que todos los 1 valdrían 0,5.

Llamaré al 1 W de Anchura. Tendré algo así:


```math
A = 0 \times W + 1 \times W + 4 \times W + 9 \times W
```

A lo que le puedo sacar factor común (sacarlo fuera del bucle)

```math
A = \left( 0 + 1  + 4 + 9  \right) \times W
```


A éste área se le llama integral (o, a veces, integral de Riemman) de x^2 entre 0 y 3.

Desarrolla un programa que calcule la integral de cualquier polinomio entre un li y un ls 
dados por el usuario.


