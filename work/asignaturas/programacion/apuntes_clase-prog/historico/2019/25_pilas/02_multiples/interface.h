#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <stdio.h>
#include <stdlib.h>

#include "stack.h"

#ifdef __cplusplus
extern "C"
{
#endif
    void title ();
    void show_error (const char *mssg);
    void show_stack (struct TStack s);
#ifdef __cplusplus
}
#endif

#endif
