#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

#ifdef __cplusplus
extern "C"
{
#endif
    void title ();
    void show_error (const char *mssg);
    void show_queue (struct TQueue s);
#ifdef __cplusplus
}
#endif

#endif
