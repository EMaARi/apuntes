#include <stdio.h>

#define GREEN   "\x1B[32m"
#define YELLOW  "\x1B[33m"
#define DEFAULT "\x1B[39m"

int main () {

    printf ("   " YELLOW);
    for (int col=0; col<0x10; col++)
        printf (" %X ", col);
    printf (DEFAULT "\n");

    for (int fila=2; fila<0x08; fila++) {
        printf (YELLOW " %X " DEFAULT, fila);
        for (int col=0; col<0x10; col++)
            printf (" %c ", fila * 0x10 + col);
        printf ("\n");
    }

    return 0;
}
