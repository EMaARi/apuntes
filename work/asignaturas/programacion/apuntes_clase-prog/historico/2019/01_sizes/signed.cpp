#include <stdio.h>


int main () {
    /*
     *  7654 3210
     *  1000 0001
     *  8421 8421
     *  8    1
     */
    unsigned char i;

    for (i=0x20; i<0x80; i++)
        printf ("%Xh: %c\n", i, i);

    return 0;
}
