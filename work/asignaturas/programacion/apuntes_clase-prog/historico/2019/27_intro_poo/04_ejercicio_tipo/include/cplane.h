#ifndef __CPLANE_H__
#define __CPLANE_H__

#include "cvehicle.h"

class CPlane : public CVehicle
{

    public:
    CPlane ();
    CPlane (
            double velMax,
            double fuelQ,
            VelUnit unitVel = VelUnit::mph,
            FuelType tFuel = FuelType::AvGas
           );

    void move ();

    ~CPlane ();
};

#endif
