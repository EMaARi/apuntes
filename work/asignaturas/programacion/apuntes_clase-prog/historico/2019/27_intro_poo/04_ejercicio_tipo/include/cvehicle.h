#ifndef __CVEHICLE_H__
#define __CVEHICLE_H__

enum class FuelType { Petrol, AvGas, Kerosene, Diesel };
enum class VelUnit  { kph, mph, knots };

class CVehicle
{
    double vmax;          // Specify the unit i velUnit
    VelUnit velUnit;

    double qFuel;         // Measured in gallons
    FuelType fuelType;

    double temp;          // Internally stored in Kelvin


    protected:
    /* pos, vel, accel */

    CVehicle ();
    CVehicle (
            double velMax,
            double fuelQ,
            VelUnit unitVel = VelUnit::knots,
            FuelType tFuel = FuelType::Petrol
            );

   ~CVehicle ();


//    /* getter & setter */
//    /* Accedentes & Mutadors */
//    double getVmax ();
//    void setVmax (double);
// 
//    double vmax (); // Variable solo lectura.

   double getTCelsius (); // Accedente a un atributo virtual.

   void virtual move () = 0;

};


#endif
