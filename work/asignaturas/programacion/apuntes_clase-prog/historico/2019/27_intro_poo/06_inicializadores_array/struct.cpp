
/* OBJETOS NORMALES */
// Ejemplo copiado de: https://en.cppreference.com/w/cpp/language/constructor
struct S {
    int n;
    S(int);        // Declaración de un constructor
    S() : n(7) {}  // Definición (sobreescritura) del constructor por defecto
                   // " : n (7)" es la lista de inicialización
                   // " : n (7) {}" es la implementación del cuerpo de la función
};

S::S(int x) : n (x) {} // Definición del constructor


/* INICIALIZACIÓN DE AGREGADOS. */
// Resumen de: https://en.cppreference.com/w/cpp/language/aggregate_initialization
// Desde C++ 11 las lista de inicialización se pueden hacer con llaves, además de
// los paréntesis.

/* Inicializadores Designados */
struct A {int x; int y; int z; };
A a {.y = 2, .x = 1}; // Pero esto es un error porque no sigue el orden de declaración.
A b {.x = 1, .y = 2}; // Esto está bien.
A c {.x = 1, .z = 2}; // Esto está bien aunque falta uno.

union u { int a; const char *b };
u f = { .b = "asdf" };          // Ok
u f = { .a = 1; b = "asdfg" };  // Mal. Se han definido todos los campos.

/* Arrays de Caracters */

char a[] = "abc";
unsigned char b[5]{"abc"};
// Equivalente a: unsigned char b[5] = {'a', 'b', 'c', '\0', '\0'};
struct S {
    int x;
    struct Foo {
        int i;
        int j;
        int a[3];
    };
};

S s1 = { 1, {2, 3, {4, 5, 6 } } };
S s2 = { 1, 2, 3, 4, 5, 6 };         // Es idéntico,pero omitiendo las llaves.
