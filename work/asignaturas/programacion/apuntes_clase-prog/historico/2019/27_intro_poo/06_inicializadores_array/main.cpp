#include <stdio.h>
#include <stdlib.h>

#include <iostream>

using namespace std;

struct A {
    const char *nombre;
    A(const char *n): nombre(n) {};
};

struct B {
    const char nombre[10];
    B(): nombre {"Juan"} {};   // Los arrays sólo se pueden/deben inicializar con constantes.
};

// /* Un poco de sin sentido */
// struct B {
//     const char nombre[10];
//     B(const char *n): nombre {n} {};  // No se puede inicializar un array con
//                                       // una variable para no dejar huérfanas
//                                       // las celdas reservadas.
// };


int main (int argc, char *argv[]) {

    A a("Pepe");
    B b;

    cout << a.nombre << endl << b.nombre << endl;

    return EXIT_SUCCESS;
}
