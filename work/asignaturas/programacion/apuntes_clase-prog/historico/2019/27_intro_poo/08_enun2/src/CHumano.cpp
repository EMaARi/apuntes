#include "CHumano.h"

#include <iostream>

using namespace std;

void
CHumano::saluda ()
{
  cout << "Hola.\n";
}

void
CHumano::anda ()
{
  cout << "Ando como un humano.\n";
}
