/*
 * =====================================================================================
 *
 *       Filename:  cpersona.h
 *
 *    Description: Class CPersona
 *
 *        Version:  1.0
 *        Created:  02/06/20 09:46:18
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  txemagon / imasen (), txemagon@gmail.com
 *   Organization:  txemagon
 *
 * =====================================================================================
 */
#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define MAX 100

class CPersona {
    /* Attributes */
    private:
        char nombre[MAX];

    /* Methods */
    public:
        CPersona () = delete;
        CPersona (const char *nom);
        void saluda ();

};

#endif
