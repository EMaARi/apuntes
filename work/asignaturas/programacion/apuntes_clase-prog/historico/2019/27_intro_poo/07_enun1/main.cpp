/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  Primer enunciado de POO
 *
 *        Version:  1.0
 *        Created:  02/06/20 09:39:56
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  txemagon / imasen (), txemagon@gmail.com
 *   Organization:  txemagon
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include	<time.h>

#include	"cpersona.h"

#define N   10			/* Número de personas */

const char *santoral[] = {
  "José",
  "Luis",
  "Santiago",
  "Francisco",
  "David",
  "Salomón",
  "Clodoveo",
  "Ramón",
  "Juan",
  "Ovidio",
  NULL
};


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  init_persona
 *  Description:  Creates a new person and gives it a name
 * =====================================================================================
 */
void
init_persona (CPersona * p[N])
{
  int c_nombre = sizeof (santoral) / sizeof (char *) - 1;

  srand (time (NULL));

  for (int i = 0; i < N; i++)
    p[i] = new CPersona (santoral[rand () % c_nombre]);
}				/* -----  end of function init_persona  ----- */


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  greet
 *  Description:  Invokes CPersona#greet
 * =====================================================================================
 */
void
greet (CPersona * p[N])
{
  for (int i = 0; i < N; i++)
    p[i]->saluda ();
}				/* -----  end of function greet  ----- */


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  destroy_persona
 *  Description:  Clears memory occupied by people
 * =====================================================================================
 */
void
destroy_persona (CPersona * p[N])
{
  for (int i = 0; i < N; i++)
    delete p[i];
}				/* -----  end of function destroy_persona  ----- */

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  Creates and makes greeting to 10 people
 * =====================================================================================
 */
int
main (int argc, char *argv[])
{
  CPersona *p[N];

  init_persona (p);
  greet (p);
  destroy_persona (p);

  return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
