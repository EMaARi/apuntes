#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__

#include "common.h"
#include "euclides.h"

#define MAXLIN 0x30

extern char prog_title[MAXLIN];

#ifdef __cplusplus
extern "C" {
#endif
    void title ();
    enum TBase ask_option ();
    void ask_vector (double v[DIM]);
    void print_vector (
        const char *label, double v[DIM],
        enum TBase base );
#ifdef __cplusplus
}
#endif

#endif
