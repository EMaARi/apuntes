#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "common.h"
#include "interfaz.h"
#include "euclides.h"

int main (int argc, char *argv[]) {

    enum TBase vec1bas, vec2bas;
    double vec1[DIM],
           vec2[DIM],
           escalar = 0,
           alpha;


    /* Entrada de Datos */
    title ();
    ask_vector(vec1);
    ask_vector(vec2);

    /* Cálculos */
    for (int i=0; i<DIM; i++)
        escalar += vec1[i] * vec2[i];

    alpha = rad2deg (acos ( escalar / module (vec1) / module (vec2) ));

    /* Salida de Datos */
    printf ("Producto Escalar:");
    print_vector ("v1", vec1, cart);
    print_vector ("v2", vec2, cart);
    printf ("Resultado = %.2lf", escalar);

    printf ("\n");
    printf ("Ángulo entre los dos: %.2lf\n", alpha);

    printf ("\n");

    return EXIT_SUCCESS;
}

