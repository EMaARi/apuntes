#ifndef __EUCLIDES_H__
#define __EUCLIDES_H__

#include "common.h"

#ifndef __cplusplus
extern "C"
{
#endif

    /* Funciones escalares */
    double deg2rad (double deg);
    double rad2deg (double rad);

    /* Funciones vectoriales */
    void car2cil (double dst[DIM], double src[DIM]);
    void car2esf (double dst[DIM], double src[DIM]);

    void cil2esf (double dst[DIM], double src[DIM]);
    void cil2car (double dst[DIM], double src[DIM]);

    void esf2car (double dst[DIM], double src[DIM]);
    void esf2cil (double dst[DIM], double src[DIM]);

#ifndef __cplusplus
}
#endif

#endif
