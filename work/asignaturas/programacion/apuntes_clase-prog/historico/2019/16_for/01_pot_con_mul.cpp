#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    int base;
    unsigned exponente;
    int resultado = 1;

    printf ("Base: ");
    scanf (" %i", &base);
    printf ("Exponente: ");
    scanf (" %u", &exponente);

    for (unsigned mul=0; mul<exponente; mul++)
        resultado *= base;    /* Esta parte la podemos sustituir por el algoritmo que multiplica */

    printf ("%i^%u = %i\n", base, exponente, resultado);


    return EXIT_SUCCESS;
}
