#include <stdio.h>
#include <stdlib.h>

/* Versión mejorada que permite exponentes negativos:
 * 2^(-3) = 1 / 2^3
 */
int main (int argc, char *argv[]) {
    int base,
        exponente;
    double resultado = 1;

    printf ("Base: ");
    scanf (" %i", &base);
    printf ("Exponente: ");
    scanf (" %i", &exponente);

    for (int mul=0; mul<abs(exponente); mul++)
        resultado *= base;

    if (exponente < 0)
        resultado = 1 / resultado;

    printf ("%i^%i = %.4lf\n", base, exponente, resultado);


    return EXIT_SUCCESS;
}
