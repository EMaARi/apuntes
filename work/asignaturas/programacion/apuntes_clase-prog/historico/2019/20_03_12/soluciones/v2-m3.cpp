#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include "ansi.h"

#define M 2
#define K 4
#define N 3


void title () {
    system ("clear");
    system ("toilet -fpagga --gay MATRICES");
    printf ("\n");
    printf ("Multiplicación de Matrices.\n");
    printf ("\n\n");
}

void pedir_datos (double *m, const char *label, int filas, int cols) {
    title ();
    printf ("Introduce los datos de la Matriz %s\n", label);
    printf ("\n");

    for (int f=0; f<filas; f++) {
        for (int c=0; c<cols; c++) {
            printf ("\t\t\t");
            for (int tab=0; tab<c; tab++)
                printf ("\t");
            scanf ("%lf", m + f * cols + c);  /* Aquí es imposible usar la notación de Arrays &m[f][c]. Al no saber el ancho le es imposible calcular el stride. */
            GO_UP(1);
        }
        printf ("\n");
    }
}

void mostrar (double *m, const char *label, int filas, int cols) {

    printf ("Matriz %s:\n", label);
    printf ("\n");
    for (int f=0; f<filas; f++) {
        printf ("\t\t");
        for (int c=0; c<cols; c++)
            printf ("%9.2lf", *(m + f * cols + c)); /* Idem con la notación de Arrays*/
        printf ("\n");
    }
}

int main (int argc, char *argv[]) {

    double A[M][K], B[K][N], C[M][N];

    bzero (C, sizeof(C));  /* Poner todo a 0 */
    pedir_datos ((double *) A, "A", M, K);   // Dos maneras
    pedir_datos (&B[0][0], "B", K, N);       // de hacer el casting

    /* Para calcular la fila i y j de c, sabemos que cij = aik·bkj contrayendo k en una acumulación */
    for (int i=0; i<M; i++)
        for (int j=0; j<N; j++)
            for (int k=0; k<K; k++)
                C[i][j] += A[i][k] * B[k][j];

    title ();
    mostrar ((double *) A, "A", M, K);  // Tb se puede usar el operador &
    mostrar ((double *) B, "B", K, N);
    mostrar ((double *) C, "C", M, N);
    printf ("\n\n");

    return EXIT_SUCCESS;
}
