#include <stdio.h>
#include <stdlib.h>

const char *simbolo = "♠";

int main (int argc, char *argv[]) {
    unsigned altura;

    // Saber cuál es la altura;
    printf ("Altura: ");
    scanf (" %u", &altura);

    // Pintar todas / cada linea;
    for (int f=0; f<altura; f++) {
       // Pintar todos / cada simbolo;
       for (int c=0; c<altura; c++)
           if (f==0 || c==0 ||
               f == altura - 1 || c == altura - 1)
               printf ("%s", simbolo);
           else
               printf (" ");
       // Imprimir un salto de linea;
       printf ("\n");
    }

    return EXIT_SUCCESS;
}
