#include <stdio.h>
#include <stdlib.h>

void dame (double a[5], int p) {
    printf ("%.2lf\n", a[p]);
}

int main (int argc, char *argv[]) {
    double v[2][3] = {
        {1, 2, 3},
        {4, 5, 6}
    };

    dame(v[0], 4);

    return EXIT_SUCCESS;
}
