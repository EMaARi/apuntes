#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

/*
 * Cogiendo la idea desarrollada en el boceto anterior (v2)
 * vamos a hacer el bucle.
 */

void show (char **lista, int s){
    for (int i=0; i<s; i++)
        printf ("%s\n", *(lista + i));
    printf ("\n");
}

int main (int argc, char *argv[]) {

    char **lista = NULL;
    int s = 0, // s: summit. cima de la pila. Primera celda vacía.
        l = 0; // l: Numero de elementos leídos por scanf.


    do {
        lista = (char **) realloc (lista, (s+1) * sizeof (char *));
        printf ("Nombre: ");
        l = scanf ("%m[^\n]", &lista[s++]); // Ojo con no poner el espacio después de las comillas.
        __fpurge (stdin);                   // sino se come el intro.
        if (l)
            show (lista, s);
    } while (l != 0);

    /* Como el último scanf no leyò nada vamos
     * a minorar el espacio reservado.  */
    lista = (char **) realloc (lista, (--s) * sizeof (char *));

    /* Ya se ha terminado de leer.
     * Aquí el programa puede hacer sus cosillas. */

    /* Liberar 2 tipos cosas */
    for (int i=0; i<s; i++)
        free (lista[i]); // la cadena reservada por scanf
    free (lista);        // El espacio cogido por realloc.

    /* ¡ Haceos el dibujo ! */

    return EXIT_SUCCESS;
}
