#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAX        0x10   /* Máximo de letras en una palabra */
#define MAX_INTENT   10   /* Número máximo de errores */

#ifndef NDBG
#define DEBUG(...)                           \
    fprintf (stderr, "\x1B[32m");            \
    fprintf (stderr, "%s\n", __VA_ARGS__);   \
    fprintf (stderr, "\x1B[0m");
#else
#define DEBUG(...)
#endif

const char *palabra[] = {
    "pergamino",
    "maceta",
    "televisor",
    "valora",
    "telefono",
    "bicicleta",
    "mueble",
    "padel",
    "estacion",
    "boxeo",
    "cacharro",
    "teclado",
    "pelota",
    "automovil",
    "caravana",
    NULL
};

unsigned npalabras = sizeof (palabra) / sizeof (char *) - 1;


void
imprimir_todas () {
    for (int i=0; palabra[i]!=NULL; i++)
        printf ("%s\n", palabra[i]);
}

void
imprimir (unsigned pos) {
    printf (
            "%s\n",
            palabra[pos % npalabras]
            );
}

void title () {
    system ("clear");
    system ("toilet -fpagga --gay '   AHORCADO   '");
    printf ("\n\n\n");
}


void
presenta (unsigned lon, char palabra[MAX]) {

    DEBUG(palabra);
    printf ("\t\t");
    for (unsigned i=0; i<lon; i++) {
        if (palabra[i])
            printf ("%c", palabra[i]);
        else
            printf ("_");

        printf (" ");
    }

    printf ("\n\n\n");
}

/* Define las funciones ha_ganado y ha_perdido
 * para que encajen con las llamadas de main
 *
 * Para realizar la función ha ganado mira el
 * manual para la función strcmp.
 * Acuérdate de que si escribes strcmp en el vim
 * y con el cursor sobre ella pulsas la letra 'K'
 * salta automáticamente el manual.
 **/

unsigned
aciertos (unsigned turnos, unsigned  errores) {
    return turnos - errores;
}

int
main (int argc, char *argv[])
{
    /*  DECLARACIÓN DE VARIABLES */
    unsigned pos;
    char     adivinado[MAX];
    size_t   lon;            // size_t es un alias de unsigned generalmente.
                             // lon: Número de carácteres de la palabra elegida.
    unsigned turnos   = 0,
             errores  = 0;

    char nueva_letra;


    /* INICIALIZACIÓN */
    bzero (adivinado, sizeof (adivinado));

    srand (time (NULL));
    pos = rand () % npalabras;
    lon = strlen (palabra[pos]);

    /* BUCLE DE JUEGO */
    do {
        title ();
        DEBUG(palabra[pos]);
        // Pedir una nueva_letra
        // Incrementar el turno

        /* Si la letra no está en la palabra
         * if (esta (nueva_letra, palabra) )
         *   -> Incrementar los aciertos
         *   -> Insertar la nueva letra en todas las posiciones que correspondan.
         * sino
         *   -> Incrementar los errores
         */

        presenta (lon, adivinado);
    } while (!ha_perdido (errores, MAX_INTENT) && !ha_ganado (adivinado, palabra[pos])); // Si no ha ganado ni ha perdido: sigue jugando.

    return EXIT_SUCCESS;
}
