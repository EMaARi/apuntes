#include <stdio.h>
#include <stdlib.h>



void
busca1 (char letra, const char *palabra) {
    for (int i=0; palabra[i]!='\0'; i++)
        if (palabra[i] == letra)
            printf ("%i: %c\n", i, palabra[i]);
}

void
busca2 (char letra, const char *palabra) {
    const char *p = palabra;

    while (*p != '\0') {
        if (*p == letra)
            printf ("%p: %c\n", p, *p);
        p++;
    }
}

void
sustituye1 (char nueva, char letra, char palabra[])
{
    for (int i=0; palabra[i]!='\0'; i++)
        if (palabra[i] == letra)
            palabra[i] = nueva;

}

void
sustituye2 (char nueva, char letra, char *p)
{
    while (*p != '\0') {
        if (*p == letra)
            *p = nueva;
        p++;
    }
}

int
main (int argc, char *argv[])
{
    char palabra[] = "supercalifragilisticoespialidosoyole";

    printf ("%s\n", palabra);
    busca1 ('i', palabra);
    printf ("\n");
    busca2 ('i', palabra);

    printf ("\n");
    sustituye1 ('0', 'o', palabra);
    printf ("%s\n", palabra);
    sustituye2 ('!', 'i', palabra);
    printf ("%s\n", palabra);

    return EXIT_SUCCESS;
}
