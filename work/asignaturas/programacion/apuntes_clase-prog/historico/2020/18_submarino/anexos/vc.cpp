#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    int    *pint;
    double *pdouble;

    void *compuesta = malloc ( sizeof (int) + sizeof (double) );

    pint    = (int *)     compuesta;
    // Versión en bytes
    pdouble = (double *)  ((char *) compuesta + sizeof(int));
    // Alternativa:
    // pdouble = (double *) (pint + 1);

    printf ("Integer: ");
    scanf  (" %i", pint);

    printf ("Real: ");
    scanf  (" %lf", pdouble);


    printf ("\n");
    printf ("Integer = %i\n",  *pint   );
    printf ("Real    = %lf\n", *pdouble);
    printf ("\n");

    for (
            char   *dump = (char *) compuesta;
            dump - (char *) compuesta < 0xC;
            dump++
            )
        printf ( "%02X ", *dump );
    printf ("\n");
    printf ("\n");

    free (compuesta);

    return EXIT_SUCCESS;
}
