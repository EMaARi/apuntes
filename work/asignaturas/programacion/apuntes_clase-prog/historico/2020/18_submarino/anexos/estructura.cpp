#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 32

struct TEmpleado {
    char nombre[N];
    double sueldo;
};

// Alias
typedef struct TEmpleado Empleado;

// Estructura Anónima
struct {
    char nombre[N];
    double sueldo;
} empleado;

typedef struct {
    char nombre[N];
    double sueldo;
} TipoEmpleado;


// Campos y de registros
// Asignación posible.
// Pero OjO con los punteros.

int
main (int argc, char *argv[])
{
    struct TEmpleado empleado;

    strcpy (empleado.nombre, "Pepe Mari");
    empleado.sueldo = 1900;

    return EXIT_SUCCESS;
}
