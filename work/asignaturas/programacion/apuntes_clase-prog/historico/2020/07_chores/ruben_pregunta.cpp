#include <stdio.h>
#include <stdlib.h>

#define N 0x20

int
main (int argc, char *argv[])
{
    char nombre[N];    // String
    unsigned dia, mes; // unsigned int
    int      anio;     // entero
    char     letra;    // carácter

    printf ("Nombre: fecha de nacimiento: letra favorita : Número de la suerte: ");

    scanf ( " %s : %u / %u / %i : %c : %*i",
            nombre, &dia, &mes, &anio, &letra );
    /*
     * Espacio : => Saca los whitespaces iniciales.
     * %s      : => String. Cadena de caracteres.
     * %u      : => Unsigned int
     * %i      : => int
     * *       : => Carácter de supresión de asignación.
     */

    printf ("%s, torpedo!\n"
            "Naciste el %u del %u en el %i después de los dolores.\n"
            "Te vi a decirl dos cosarls:\n"
            "\t Lo primerorl que tu letra favorita es la %c.\n"
            "\t Y lo segundorl que tú número favorito es %s.\n\a\a\a",
            nombre, dia, mes, anio, letra, "una mierda");


    return EXIT_SUCCESS;
}
