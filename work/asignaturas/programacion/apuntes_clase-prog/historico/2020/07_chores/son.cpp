#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>


#define NUMIN 10
#define NUMAX 100



// Cómo crear alias.
typedef int entero;

void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga NUMERO");
    printf ("\n\n");
}


void
ver_numero (unsigned num)
{
    printf (
            "\n"
            "=============\n"
            "%7u\n"
            "=============\n"
            "\n",
            num
            );
}

unsigned
aleatorio_entre (unsigned min, unsigned max)
{
    unsigned cuantos = max - min + 1;
    return rand () % cuantos + min;
}

void
quieres_otro (char *r)
{
    printf ("\n");
    printf ("Otro ? (s/n): ");
    scanf  ("%c", r);
}

void
quiere_otro (char *r)
{
    *r = 'a';

    printf ("\n");
    printf ("Otro ? (s/n): ");
    scanf  ("%1[sn]", r);
    __fpurge (stdin);
}

int
main (int argc, char *argv[])
{
    char respuesta;

    srand (time (NULL));

    do {

        titulo ();

        ver_numero (aleatorio_entre (NUMIN, NUMAX));
        do {
            quiere_otro ( &respuesta );
        } while ( respuesta != 's' && respuesta != 'n' );

    } while (tolower (respuesta) == 's');

    return EXIT_SUCCESS;
}
