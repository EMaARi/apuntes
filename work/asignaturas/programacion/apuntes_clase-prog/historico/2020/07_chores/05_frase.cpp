#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 0x20U

void
imprimir (char nom[], unsigned length) {
    printf ("| ");
    for (unsigned celda=0; celda<length; celda++)
        printf ("%c | ", nom[celda]);

    printf ("\n");
}


int
main (int argc, char *argv[])
{
    char nombre[MAX];
    char *p = nombre;

    /* ENTRADA DE DATOS */
    printf ("Nombre: ");
    scanf  (" %s", nombre);

    printf ("nombre = %p\n", nombre);

    imprimir (nombre, MAX);
    imprimir (nombre, strlen (nombre));

    while (*p != '\0'){
        if ( *p != 'i' )
            printf ("%c", *p);
        else
            printf ("!");
        p++;
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
