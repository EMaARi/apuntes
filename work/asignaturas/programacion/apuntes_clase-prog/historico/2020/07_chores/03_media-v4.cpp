#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <ctype.h>



void
error ()
{
    fprintf (stderr, "Has metido un número mal.\n");
    exit (EXIT_FAILURE);
}

void
ver_media (double media) {
    printf ("===============\n"
            "  Media: %.2lf\n"
            "===============\n"
            , media);
    printf ("\n");
}

int
main (int argc, char *argv[])
{
    char     otra;
    unsigned veces = 0;
    double   entrada,
             suma = 0;


    do {
        printf ("Número %3u: ", veces+1);
        __fpurge (stdin);
        if (scanf (" %lf", &entrada) == 0)
            error ();

        veces++;
        suma += entrada;

        printf ("Otro (S/n): ");
        __fpurge (stdin);
        scanf ("%c", &otra);
    } while (tolower (otra) != 'n');

    ver_media ( suma / veces );

    return EXIT_SUCCESS;
}
