#include <stdio.h>
#include <stdlib.h>

#ifndef NDBG
#define DEBUG(valor1, valor2)                \
    printf ("\x1B[32m");                     \
    printf ("\n%u => %u\n", valor1, valor2); \
    printf ("\x1B[0m");
#else
#define DEBUG(valor1, valor2)
#endif

int
divide (unsigned divisor, unsigned dividendo) {
    return dividendo % divisor == 0;
}

void
titulo () {
    system ("clear");
    system ("toilet -fpagga PERFECTOS");
    printf ("\n\n\n");
}

int
main (int argc, char *argv[])
{
    unsigned num,        // Posible numero perfecto.
             suma = 0;

    /* ENTRADA DE DATOS */
    titulo ();
    printf ("Introduce un número: ");
    scanf (" %u", &num);

    for ( unsigned pd=num/2; pd>0; pd--)
        if (divide(pd, num)) {
            suma += pd;
            DEBUG(pd, num);
        }

    DEBUG(num, suma);

    if (suma == num)
        printf ("%u es perfecto.", num);
    else
        printf ("%u no es perfecto.", num);

    printf ("\n");

    return EXIT_SUCCESS;
}
