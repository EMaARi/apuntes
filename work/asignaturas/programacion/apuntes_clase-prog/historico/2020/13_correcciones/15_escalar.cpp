#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N     5
#define MAX 200

void
rellenar (double A[N])
{
    for (int i=0; i<N; i++)
        A[i] = (rand () % 100 * (MAX + 1)) / 100.  - MAX / 2;
}

void
imprimir (const char *nombre, double A[N])
{
    printf ("\x1B[32;1m%s\x1B[0m\n", nombre);

    /* Linea Superior Recuadro*/
    printf ("┌──────────");
    for (int i=0; i<N-2; i++)
        printf ("┬──────────");
    printf ("┬──────────┐\n");

    /* Impresión Matriz */
    for (int i=0; i<N; i++)
        printf ("│ %7.2lf  ", A[i]);
    printf ("│\n");

    /* Linea Inferior Recuadro */
    printf ("└──────────");
    for (int i=0; i<N-2; i++)
        printf ("┴──────────");
    printf ("┴──────────┘\n");

    printf ("\n");
}

int
main (int argc, char *argv[])
{
    double A0[N], A1[N], suma = 0;

    srand (time (NULL));

    /* VALORES INICIALES */
    rellenar (A0);
    rellenar (A1);

    imprimir ("A0", A0);
    imprimir ("A1", A1);

    for (int i=0; i<N; i++)
        suma += A0[i] * A1[i];

    printf ("\x1B[34;1mEscalar = %.2lf\x1B[0m\n\n", suma);

    return EXIT_SUCCESS;
}
