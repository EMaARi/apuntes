#include <stdio.h>
#include <stdlib.h>

#define NCOEF 6
#define GRADO (NCOEF - 1)

#define STEP .001

double
f (double p[NCOEF], double x) {
    double valor = 0;
    double pot   = 1;

    for (int coef=0; coef<NCOEF; coef++) {
        valor += pot * p[coef];
        pot *= x;
    }

    return valor;
}

int
main (int argc, char *argv[])
{
    double p[NCOEF] = { 3, 2, 5, 1 };

    double valor1, valor2, derivada;

    for (double x=-10; x<10; x+=STEP) {
        valor1 = f(p, x);
        valor2 = f(p, x + STEP);
        derivada = (valor2 - valor1) / STEP;
        printf ("%.4lf\t%.4lf\t%.4lf\n", x, valor1, derivada);
    }

    return EXIT_SUCCESS;
}
