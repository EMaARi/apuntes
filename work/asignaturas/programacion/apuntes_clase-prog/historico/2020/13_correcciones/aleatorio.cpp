#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

int
main (int argc, char *argv[])
{
    int A[N];

    srand (time(NULL));

    for (int i=0; i<N; i++)
        A[i] = rand () % 10 + 1;

    return EXIT_SUCCESS;
}
