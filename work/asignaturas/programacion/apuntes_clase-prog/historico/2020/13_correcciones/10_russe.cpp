#include <stdio.h>
#include <stdlib.h>


bool
es_impar (unsigned n) {
    return n % 2 == 1;
}

void
imprimir (unsigned resultado) {
    printf ("\n\n"
            "===============\n"
            "    %u\n"
            "===============\n",
            resultado
            );
}

int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned op1, op2, suma = 0;

    /* ENTRADA DE DATOS */
    printf ("Operando 1: ");
    scanf (" %u", &op1);

    printf ("Operando 2: ");
    scanf (" %u", &op2);

    while (op2 > 0) {
        if ( es_impar (op2) )
            suma += op1;
        op1 *= 2;
        op2 /= 2;
    }

    imprimir (suma);

    return EXIT_SUCCESS;
}
