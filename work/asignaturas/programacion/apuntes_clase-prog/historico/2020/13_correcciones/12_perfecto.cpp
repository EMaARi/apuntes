#include <stdio.h>
#include <stdlib.h>

int
divide (unsigned divisor, unsigned dividendo) {
    return dividendo % divisor == 0;
}


int
main (int argc, char *argv[])
{
    unsigned num,        // Posible numero perfecto.
             suma = 0;

    /* ENTRADA DE DATOS */

    for (unsigned num=1; num<64000; num++) {
        suma = 0;
        for ( unsigned pd=num/2; pd>0; pd--)
            if (divide(pd, num))
                suma += pd;


        if (suma == num)
            printf ("%u\n", num);

    }
    return EXIT_SUCCESS;
}
