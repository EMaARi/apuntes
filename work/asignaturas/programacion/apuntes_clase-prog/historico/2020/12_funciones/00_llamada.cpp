#include <stdio.h>
#include <stdlib.h>

/*
 * Este programa está hecho con propósitos
 * depurativos
 */

// La declaración se compone de:
// 1. Un tipo de dato asociado al valor de retorno.
// 1. Un identificador.
// 1. Los parámetros formales
int suma (int op1, int op2) {
    return op1 + op2;
}

int
main (int argc, char *argv[])
{
    int a = 2,
        b = 5,
        c;

    // Llamada a la función suma
    // Los valores suministrados
    // se llaman parámetros actuales.
    c = suma (2 * a + 3, b);

    printf ("c = %i\n", c);

    return EXIT_SUCCESS;
}
