/*
 * =====================================================================================
 *
 *       Filename:  01_simple.cpp 1.0 19/01/21 16:36:26
 *
 *    Description:  Solución al ejercicio 10
 *    sea h=5
 *
 *    *(CR)
 *    **(CR)
 *    ***(CR)
 *    ****(CR)
 *    *****(CR)
 *
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>






void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay TRIANGULOS");
    printf ("\n\n\n");
}



unsigned
pedir_altura ()
{
    unsigned altura;

    printf ("Altura: ");
    scanf (" %u", &altura);

    return altura;
}


/* Depura esta función o haz un seguimiento línea a línea */
void
triangulo (unsigned h)
{
    for (unsigned f=0; f<h; f++) {
        for (unsigned c=0; c<f+1; c++)  // Fíjate en cómo difiere esta línea de la del cuadrado
            printf ("*");               // f+1 indica que en la fila 0 hay que poner 1 columna.
        printf ("\n");
    }

    printf ("\n\n");
}




int
main (int argc, char *argv[])
{
    unsigned h;             // Altura del triángulo.

    titulo ();
    h = pedir_altura ();
    triangulo (h);          // También hubiéramos podido resumir esta línea
                            // triangulo ( pedir_altura () );

    return EXIT_SUCCESS;
}
