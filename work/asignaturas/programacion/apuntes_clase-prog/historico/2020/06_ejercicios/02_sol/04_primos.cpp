/*
 * =====================================================================================
 *
 *       Filename:  primos.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Calculo de los 100 primeros números primos.
 *                  Parte III: Un pequeño refactor.
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>


#define BUSCADOS    100

void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay PRIMOS");
    printf ("\n\n");

}





void
poner_resultado ( unsigned primo[BUSCADOS])
{
    for (unsigned i=0; i<BUSCADOS; i++)
        printf ("%u  ", primo[i]);

    printf ("\n\n");
}




int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned posible_primo,
             tiene_divisores = 0,
             numero_de_primo = 0;

    unsigned primo[BUSCADOS];



    /* ENTRADA Y SALIDA DE DATOS */
    titulo ();


    /* CÁLCULOS */
    for (  unsigned posible_primo=2; numero_de_primo<BUSCADOS; posible_primo++) {

        tiene_divisores = 0;
        for (unsigned posible_div=posible_primo-1; posible_div>1; posible_div--)
            if (posible_primo % posible_div == 0)
                tiene_divisores = 1;

        if (!tiene_divisores)
            primo[numero_de_primo++] = posible_primo; // El operador ++ de postfijo usa el valor de la variable y a posteriori lo incrementa.
    }


    /* SALIDA DE DATOS */
    poner_resultado (primo);


    return EXIT_SUCCESS;
}
