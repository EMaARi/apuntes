#include <stdio.h>
#include <stdlib.h>

unsigned
pedir_lado () {
    unsigned lado;

    printf ("lado: ");
    scanf (" %u", &lado);

    return lado;
}

/* Las funciones que devuelven verdadero o falso se llaman predicados. */
bool /* es un tipo de dato de C++ que vale true o false. No es estrictamente c */
es_borde (unsigned f, unsigned c, unsigned l)
{
    /* Dale una pensada a esta expresión.
     * Si no la entiendes, haz el árbol gramatical
     **/
    return f == 0     ||        // fila superior
           f == l - 1 ||        // fila inferior
           c == 0     ||        // borde izquierdo
           c == l - 1;          // borde derecho
}

int
main (int argc, char *argv[])
{
    unsigned l;

    l = pedir_lado ();

    for (unsigned fila=0; fila<l; fila++) {
        for (unsigned col=0; col<l; col ++)
            if (es_borde (fila, col, l))
                printf ("*");
            else
                printf (" ");
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
