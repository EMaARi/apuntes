#include <stdio.h>
#include <stdlib.h>

#define M 3

int
mira (int A[M][M], int f, int c) {
    return A[f % M][c % M];
}

int
main (int argc, char *argv[])
{
    int suma = 0, multiplicacion;

    int A[M][M] = {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };

    /* Diagonales principales */
    for (int fila=0; fila<M; fila++) {
        multiplicacion = 1;
        for (int despl=0; despl<M; despl++)
            multiplicacion *= mira (A, fila + despl, despl);
        suma += multiplicacion;
    }

    /* Diagonales secundarias */
    for (int fila=0; fila<M; fila++) {
        multiplicacion = 1;
        for (int despl=0; despl<M; despl++)
            multiplicacion *= mira (A, fila + despl, M - 1 - despl);
        suma -= multiplicacion;
    }

    printf ("Determinante = %i\n", suma);

    return EXIT_SUCCESS;
}
