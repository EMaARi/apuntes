#include <stdio.h>
#include <stdlib.h>

#define M 2
#define K 3
#define N 4

#define SPACES 8

void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay 'MATRICES'");
}

void
pedir_datos (double *matriz, int filas, int cols, const char *label)
{
    printf ("\nIntroduce Matriz %s:\n\n", label);

    for (int f=0; f<filas; f++) {
        printf ("\n\t");
        for (int c=0; c<cols; c++) {
            printf ("\t\x1B[s");
            scanf (" %lf", matriz + f * cols + c);
            printf ("\x1B[u");
        }
    }

    printf ("\n\n");

}

void
imprimir_matriz (double *matriz, int filas, int cols, int y, int x)
{
    for (int f=0; f<filas; f++){
        printf ("\x1B[%i;%if", y + f, x);
        for (int c=0; c<cols; c++)
            printf ("\x1B[%i;%if%8.2lf", y + f, x + c * SPACES, *(matriz + f * cols + c));
        printf ("\n");
    }
}

void
print (int y, int x, const char *label) {
    printf ("\x1B[%i;%if%s", y, x, label);
}

int
main (int argc, char *argv[])
{
    double A[M][K], B[K][N], C[M][N];

    titulo ();
    pedir_datos ( (double *) A, M, K, "A");
    pedir_datos ( (double *) B, K, N, "B");
    titulo ();
    imprimir_matriz ( (double *) A, M, K, 8, 5 );
    imprimir_matriz ( (double *) B, K, N, 8, 35 );

    for (int i=0; i<M; i++)
        for (int j=0; j<N; j++) {
            C[i][j] = 0;
            for (int k=0; k<K; k++)
                C[i][j] += A[i][k] * B[k][j];
        }

    imprimir_matriz ( (double *) C, M, N, 8, 75 );

    print (5, 16, "Matriz A");
    print (5, 49, "Matriz B");
    print (5, 89, "Matriz C");
    print (9, 33, "x");
    print (9, 71, "=");
    print (15, 1, "");

    return EXIT_SUCCESS;
}
