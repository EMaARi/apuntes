#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void imprimir (int *numero, int cuantos) {
    for (int i=0; i<cuantos; i++)
        printf ("%4i ", numero[i]);
    printf ("\n");
}


int
main (int argc, char *argv[])
{
    int *numero = NULL;
    int cuantos;
    int aux;

    /* Inicialización */
    srand (time (NULL));

    /* Entrada de Datos */
    printf ("¿Cuántos números quieres que tenga el vector? ");
    scanf (" %i", &cuantos);

    /* Asignar valores iniciales */
    numero = (int *) malloc (cuantos * sizeof (int));

    for (int i=0; i<cuantos; i++)
        numero[i] = random () % cuantos + 1;

    imprimir (numero, cuantos);

    int cual, la_menor_pos;
    /* Ordenar por el algoritmo Berbiela */
    for (int menor=0; menor<cuantos-1; menor++){
        for (cual=menor+1,
                la_menor_pos=menor; cual<cuantos; cual++)
            if (numero[cual] < numero[la_menor_pos])
                la_menor_pos = cual;

        aux = numero[menor];
        numero[menor] = numero[la_menor_pos];
        numero[la_menor_pos] = aux;
    }

    imprimir (numero, cuantos);

    free (numero);

    return EXIT_SUCCESS;
}
