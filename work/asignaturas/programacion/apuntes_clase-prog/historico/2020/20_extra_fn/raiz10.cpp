#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define LIM 20
int
main (int argc, char *argv[])
{
    double r=10;

    for (int i=0; i<LIM; i++) {
        r = 10 * sqrt(r);
        printf ("Paso %3i => %lf\n", i, r);
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
