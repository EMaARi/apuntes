#include <stdio.h>
#include <stdlib.h>

const char *palindromo = "dabale arroz a la zorra el abad";

void
imprime (const char *l) {
    if ( *l == '\0' )
        return;
    imprime (l+1);
    printf ("%c", *l);
}

int
main (int argc, char *argv[])
{

    imprime (palindromo);
    printf ("\n");

    return EXIT_SUCCESS;
}
