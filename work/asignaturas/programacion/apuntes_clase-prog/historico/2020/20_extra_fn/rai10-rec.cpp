#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define LIM 20

double raiz (double val, int veces) {

    if (veces == 0)
        return 10;

    return 10 * sqrt(raiz (val, veces-1));
}

    int
main (int argc, char *argv[])
{
    printf ("\n");

    printf ("%lf\n\n", raiz (10, 20));

    return EXIT_SUCCESS;
}
