#include <stdio.h>
#include <stdlib.h>

/*
+--------------+                     +----------------+                   +-----------------------+
|              |                     |                |                   |                       |
|      --------+-------------------->|        --------+------------------>|  code                 |
|              |                     |                |                   |                       |
+--------------+                     +----------------+                   |                       |
    pfunc                                   func                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          |                       |
                                                                          +-----------------------+
*/

int suma (int op1, int op2) { return op1 + op2; }

int
main (int argc, char *argv[])
{

    // Antes era viable porque coinciden los tipos de datos
    int misuma (int, int);

    printf ("2 + 3 = %i\n", misuma (2,3));

    return EXIT_SUCCESS;
}
