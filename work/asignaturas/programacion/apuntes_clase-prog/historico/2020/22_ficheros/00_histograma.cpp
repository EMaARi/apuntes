#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <strings.h>


#define NUM_LET ('z' - 'a' + 1)

void
compute (const char *pletter, double counter[NUM_LET]) {
    unsigned total = 0;

    bzero (counter, NUM_LET * sizeof (unsigned));

    while (*pletter != '\0') {
        if (isalpha(*pletter))
            counter[tolower(*pletter)-'a']++;
        total++;
        pletter++;
    }

    /* Normalize counter */
    for (int i=0; i<NUM_LET;i++)
        counter[i] /= total;

}

void
print (double counter[NUM_LET]){
    printf ("\n");
    for (int i=0; i<NUM_LET; i++)
        printf ("%c: %5.2lf%%\n", i + 'A', ((int) (counter[i] * 10000)) / 100.);
    printf ("\n");
}

int
main (int argc, char *argv[])
{
    FILE *pf;
    long size;

    const char *text = NULL;
    double counter[NUM_LET];

    // Conectar un stream a un fichero
    pf = fopen ("fortune.txt", "r");
    if (!pf) {
        fprintf (stderr, "File not found.\n");
        return EXIT_FAILURE;
    }

    // Calcular Tamaño del Fichero
    fseek (pf, 0, SEEK_END);
    size = ftell(pf);
    rewind (pf);

    // Cargamos en memoria el fichero
    text = (const char *) malloc (size);
    fread ((void *) text, size, 1, pf);

    compute(text, counter);
    print (counter);

    fclose (pf);

    return EXIT_SUCCESS;
}
