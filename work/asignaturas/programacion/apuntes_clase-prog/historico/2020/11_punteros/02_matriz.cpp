#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define F 6
#define C 9

void
imprimir(char cuadro[F][C]) {
    for (int f=0; f<F; f++){
        printf ("\t");
        for (int c=0; c<C; c++)
            printf ("%c  ", cuadro[f][c]);
        printf ("\n");
    }
}

void
inicializa (char cuadro[F][C]) {

    /* INICIALIZACIÓN */
    memset (cuadro, '0', F * C);

    for (int c=0; c<='z'-'a'; c++)
        cuadro[0][c] = 'a' + c;

}

int
main (int argc, char *argv[])
{
    char cuadro[F][C];

    inicializa (cuadro);

    /* SALIDA DE DATOS */
    imprimir (cuadro);

    return EXIT_SUCCESS;
}
