#include <stdio.h>
#include <stdlib.h>

#define N 100


int
le_de_la_p_gana_al_usuario (int s, int *room)
{
    return  room[s - 1] < 0 ? 0 : 1;
}


int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    int *room;
    int summit = 0;

    room = (int *) malloc (sizeof (int));

    /* ENTRADA DE DATOS */
    do {
        printf ("Número: ");
        scanf  ("%i", &room[summit++]);
        room = (int *) realloc ( room, (summit +1) * sizeof (int) );
    } while (le_de_la_p_gana_al_usuario (summit, room));
    summit--;

    /* SALIDA DE DATOS */
    printf ("\n");
    for (int i=0; i<summit; i++)
        printf ("%i: %i\n", i+1, room[i]);
    printf ("\n");

    free (room);

    return EXIT_SUCCESS;
}
