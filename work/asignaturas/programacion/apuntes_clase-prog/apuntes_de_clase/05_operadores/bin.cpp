#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void
metodo1 (unsigned decimal)
{
    while (decimal > 0) {
        printf ("%u", decimal % 2);
        decimal >>= 1;
    }

    printf ("\n");
}

void
metodo2 (unsigned decimal)
{
    for ( ;decimal > 0; decimal >>= 1)
        printf ("%u", decimal % 2);

    printf ("\n");
}


void
metodo3 (unsigned decimal)
{
    unsigned mask = 1 << (unsigned) log2(decimal);  // downcasting

    for (; mask>0; mask >>= 1)  // mask = mask >> 1
        if ( (decimal & mask) != 0)
            printf ("1");
        else
            printf ("0");

    printf ("\n");
}


void
metodo4 (unsigned decimal)
{
    unsigned mask = 1 << (unsigned) log2(decimal);  // downcasting

    for (; mask>0; mask >>= 1)  // mask = mask >> 1
        if ( decimal & mask )
            printf ("1");
        else
            printf ("0");

    printf ("\n");
}


int
main (int argc, char *argv[])
{

    unsigned decimal;

    scanf (" %u", &decimal);

    /*
           /=2  %
    14 => 1110
     7 => 0111 (0)
     3 => 0011 (10)
     1 => 0001 (110)
     0 => 0000 (1110)
     */
    metodo3 (decimal);


    return EXIT_SUCCESS;
}
