#include <stdio.h>
#include <stdlib.h>

unsigned
pedir_lado () {
    unsigned lado;

    printf ("lado: ");
    scanf (" %u", &lado);

    return lado;
}

bool
es_borde (unsigned f, unsigned c, unsigned l)
{
    return f == 0     ||        // fila superior
           f == l - 1 ||        // fila inferior
           c == 0     ||        // borde izquierdo
           c == l - 1;          // borde derecho
}

bool
es_diagonal1 (unsigned f, unsigned c, unsigned l)
{
    return f == c;
}


bool
es_diagonal2 (unsigned f, unsigned c, unsigned l)
{
    return f + c == l - 1;
}

int
main (int argc, char *argv[])
{
    unsigned l;

    l = pedir_lado ();

    for (unsigned fila=0; fila<l; fila++) {
        for (unsigned col=0; col<l; col ++)
            if (
                    es_borde (fila, col, l) ||
                    es_diagonal1 (fila, col, l) ||
                    es_diagonal2 (fila, col, l)
                    )
                printf ("*");
            else
                printf (" ");
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
