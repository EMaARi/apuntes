/*
 * =====================================================================================
 *
 *       Filename:  04_reves.cpp 1.0 19/01/21 16:36:26
 *
 *    Description:  Solución al ejercicio 13
 *    sea h=5
 *
 *     *****(CR)
 *     ****(CR)
 *     ***(CR)
 *     **(CR)
 *     *(CR)
 *
 *
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>






void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay TRIANGULOS");
    printf ("\n\n\n");
}



unsigned
pedir_altura ()
{
    unsigned altura;

    printf ("Altura: ");
    scanf (" %u", &altura);

    return altura;
}


/* Depura esta función o haz un seguimiento línea a línea */
void
triangulo (unsigned h)
{
    for (unsigned f=h; f>0; f--) {
        for (unsigned c=0; c<f; c++)
            printf ("*");

        printf ("\n");
    }

    printf ("\n\n");
}




int
main (int argc, char *argv[])
{

    titulo ();
    triangulo (pedir_altura ());

    return EXIT_SUCCESS;
}
