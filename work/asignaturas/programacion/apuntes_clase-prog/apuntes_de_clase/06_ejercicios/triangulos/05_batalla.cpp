/*
 * =====================================================================================
 *
 *       Filename:  05_batalla.cpp 1.0 19/01/21 16:36:26
 *
 *    Description:  Solución al ejercicio 14
 *
 *      sea b=9
 *           *
 *          ***
 *         *****
 *        *******
 *      **********
 *
 *
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>






void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay TRIANGULOS");
    printf ("\n\n\n");
}



unsigned
pedir_base ()
{
    unsigned base;

    printf ("Base: ");
    scanf (" %u", &base);

    return base;
}


/* Hay varias soluciones. Voy a poner una que va por partes. */
/* Es imprescindible que razonéis los programas sobre un papel. Acordaos. */
void
triangulo (unsigned b)
{
    unsigned h = ( b + 1 ) / 2;

    for (unsigned f=0; f<h; f++) {

        /* Ponemos el triángulo de la izquierda */
        for (unsigned c=0; c<h; c++)
            if ( c + f < h - 1 )
                printf (" ");
            else
                printf ("*");

        /* Ahora el de la derecha, pero no siempre hay que poner la columna 0 */
        for (unsigned c=0;
                c < f + (2*h == b )? 1 : 0;   // Medita esta línea que contiene el operador condicional.
                c++)
            printf ("*");

        printf ("\n");
    }

    printf ("\n\n");
}




int
main (int argc, char *argv[])
{

    titulo ();
    triangulo (pedir_base ());

    return EXIT_SUCCESS;
}
