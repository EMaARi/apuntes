/*
 * =====================================================================================
 *
 *       Filename:  03_entrada.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Ahora el objetivo es proteger la entrada para que los números
 *                  estén comprendidos en un rango. (II)
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * Cuando veas números metidos en el código a pata (bootleg): mosquéate.
 * Para conceptualizar el código vamos a definir constantes.
 */

#define MIN         1
#define MAX      1000


int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned posible_primo;


    /* ENTRADA Y SALIDA DE DATOS */
    do {
        /* Borramos la pantalla y ponemos un título */
        system ("clear");                            // Con system podemos ejecutar comandos de consola.
        system ("toilet -fpagga --gay PRIMOS");

        printf ("Número [%i - %i]: ", MIN, MAX);
        scanf ("%u", &posible_primo);
    } while (posible_primo < MIN || posible_primo > MAX);

    /* CÁLCULOS */


    /* SALIDA DE DATOS */
    printf ("Has introducido el número %i\n", posible_primo);

    return EXIT_SUCCESS;
}

