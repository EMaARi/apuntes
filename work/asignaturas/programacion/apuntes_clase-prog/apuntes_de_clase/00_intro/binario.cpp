#include <stdio.h>

int main() {

    /* Declaración de Variables */
    int op1,  // Declaración de variable: Tipo de datos + identificador
        op2;

    /* Entrada de Datos */
    printf("Operando 1: ");
    scanf("%d", &op1);
    printf("Operando 2: ");
    scanf("%d", &op2);

    /* Operaciones */

    /* Salida de Datos */
    printf("%d XOR %d = %d\n", op1, op2, op1 ^ op2);

    return 0;
}
