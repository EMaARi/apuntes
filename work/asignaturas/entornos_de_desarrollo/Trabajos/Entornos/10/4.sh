#!/bin/bash

echo -e " Creando 3 directorios: \n ---------------------- \n"

for (( i=1; i<=3; i++ ))
do
  echo -e "Creando directorio_$i"
  mkdir directorio_$i
done

echo -e "\n\nTERMINADO"
