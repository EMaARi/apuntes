#!/bin/bash

echo -e " Leyendo 3 primeras líneas \n ---------------------- \n"

for (( i=1; i<=3; i++ ))
do
  echo -e "Leyendo fichero$i"
  head -3 fichero$i
done

echo -e "\n\nTERMINADO"
