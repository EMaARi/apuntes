#!/bin/bash

echo -e " Creando 40 archivos: \n ---------------------- \n"

for (( i=1; i<=40; i++ ))
do
  echo -e "Creando archivo_$i"
  touch archivo_$i
done

echo -e "\n\nTERMINADO"
