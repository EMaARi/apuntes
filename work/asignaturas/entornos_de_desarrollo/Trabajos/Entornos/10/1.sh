#!/bin/bash

echo -e " Inserta un número entre 0 al 15: \n ---------------------- \n"
read num
echo -e "\n\n"

for (( i=$num; i<=15; i++ ))
do
  echo -e "$i /15 \n"
done

echo -e "Terminado"
