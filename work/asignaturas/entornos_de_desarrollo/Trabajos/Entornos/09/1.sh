#!/bin/bash

echo -e "Mete dos numeros separados por un INTRO"
read p
echo -e "Introduce el segundo numero"
read s

echo "Has metido $p y $s"

if [ $p -eq $s ];
then
    echo "Los dos numeros son iguales"
fi
if [ $p -gt $s ];
then
    echo "$p es mayor que $s"
fi
if [ $p -lt $s ];
then
    echo "$s es mayor que $p"
fi
