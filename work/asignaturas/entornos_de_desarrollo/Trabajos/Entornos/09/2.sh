#!/bin/bash

echo -e "Mete dos palabras separados por un INTRO"
read p
echo -e "Introduce la segunda palabra"
read s

echo "Has metido $p y $s"

if [ $p = $s ];
then
    echo "$p y $s, son iguales"
fi
if [ $p != $s ];
then
    echo "$p y $s, son diferentes palabras"
fi
