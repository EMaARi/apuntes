#!/bin/bash

echo -e "Comprueba si el direcorio existe \n INTRUDUCELO A CONTINUACIÓN:"
read p

echo -e "Has metido \"$p\", \n buscando..."

if [ -d $p ];
then
    echo -e "VERIFICACIÓN CORRECTA \nEl directorio $p existe"
else
    echo -e "INCORRECTO \nEl directorio $p no existe"
fi
