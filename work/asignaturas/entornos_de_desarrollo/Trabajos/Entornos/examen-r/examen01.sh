#!/bin/bash

sum=0
re='^[0-9]+$'

while [ $sum -lt 50 ]
    do
        echo -e "Introduce un numero: ($sum/50)"
        read num
        if [[ $num =~ $re ]];
            then
                let sum=$(( sum + num ))
            else
                echo -e "[$num] no es un numero \n"
            fi
        echo -e "\n"
    done
echo -e "Introduce un numero: ($sum/50)"
