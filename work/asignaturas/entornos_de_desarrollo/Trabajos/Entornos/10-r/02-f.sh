#!/bin/bash
echo -e "Instroduce el nombre que comparar las lineas de dos archivos: \n"
read -p "Introduce primer archivo: " Parch
read -p "Introduce segundo archivo: " Sarch

RParch=$(find $Parch)
RSarch=$(find $Sarch)

Plist=$(cat $RParch | wc -l)
Slist=$(cat $RSarch | wc -l)

if [ $Plist -lt $Slist ];
    then
        echo -e "El $RParch ($Plist) es menor que $RSarch ($Slist)"
    elif [ $Slist -lt $Plist ];
    then
        echo -e "El $RSarch ($Slist) es menor que $RParch ($Plist)"
    else
        echo -e "ERROR"
    fi
