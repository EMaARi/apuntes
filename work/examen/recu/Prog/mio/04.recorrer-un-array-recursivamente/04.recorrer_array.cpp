#include <stdio.h>
#include <stdlib.h>

const char *ordinal[3][10] = {
    {"", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto", "septimo", "octavo", "noveno"},
    {"", "décimo", "vigésimo", "trigésimo", "cuatrigésimo", "quincuagésimo", "sexgesimo", "septuagésimo", "octogésimo", "nonagésimo"},
    {"", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo", "quingentésimo", "sexgentésimo", "septingentésimo", "octogentésimo", "nonigentésimo"}
};

void numero (int n) {
        /*Imprime desde el ultimo numero*/
    /*printf ("%i  ", n % 10); //Imprimir el último número
    if (n != 0)     //Si ya es 0 para
        numero (n / 10); //Te pasa al siguiente numero*/

        /*Se cambia de orden para empezar al principio*/
    if (n != 0) {     //Si ya es 0 para y no imprime el 0
        numero (n / 10); //Te pasa al siguiente numero
        printf ("%i  ", n % 10); //Imprimir el último número
    }
}

int main (int argc, char *argv[]) {

    int n = 153;

    numero (n);   //Llamada a la funcion con el valor de N

    printf ("\n");

    return EXIT_SUCCESS;
}
