#include <stdio.h>
#include <stdlib.h>

    /*Funciona el programa con solo funciones*/

/*int dividir (int dividendo, int divisor) {
    return dividendo / divisor;
}

int halving (int dividendo) {
    return dividendo >> 1;
}

int main (int argc, char *argv[]) {

    int (*p) (int, int);
    int dividendo = 8,
        divisor   = 2;

    printf (" dividir (%i,%i) = %i\n", dividendo, divisor, dividir (dividendo, divisor));
    printf (" halving (%i) = %i\n", dividendo, dividir (dividendo, divisor));

    printf ("\n");

    return EXIT_SUCCESS;
}*/

    /*------------------------------------*/


    /*Punteros a funciones V1*/

/*struct TParDiv {
    int dividendo;
    int divisor;
};

int dividir (void *parametros) {
    struct TParDiv *par = (struct TParDiv *) parametros;
    return par->dividendo / par->divisor;
}

int halving (void *parametros) {
    int *par = (int *) parametros;
    return *par >> 1;
}

int main (int argc, char *argv[]) {

    int (*p) (void *);
    int dividendo = 8,
        divisor   = 2;
    struct TParDiv par_div;

    p = &dividir;
    par_div.dividendo = dividendo;
    par_div.divisor = divisor;

    printf (" dividir (%i,%i) = %i\n", dividendo, divisor, (*p) ((void *) &par_div));
    p = &halving;
    printf (" halving (%i) = %i\n", dividendo, (*p) ((void *) &dividendo));

    printf ("\n");

    return EXIT_SUCCESS;
}*/

    /*------------------------------------*/


    /*Punteros a funciones V2*/

struct TParDiv {
    int dividendo;
    int divisor;
};

int dividir (void *parametros) {
    struct TParDiv *par = (struct TParDiv *) parametros;
    struct TParDiv p = *par;
    return p.dividendo / p.divisor;
}

int halving (void *parametros) {
    int *par = (int *) parametros;
    int p = *par;
    return *par >> 1;
}

int main (int argc, char *argv[]) {

    int (*p) (void *);
    int dividendo = 8,
        divisor   = 2;
    struct TParDiv par_div;

    p = &dividir;
    par_div.dividendo = dividendo;
    par_div.divisor = divisor;

    printf (" dividir (%i,%i) = %i\n", dividendo, divisor, (*p) ((void *) &par_div));
    p = &halving;
    printf (" halving (%i) = %i\n", dividendo, (*p) ((void *) &dividendo));

    printf ("\n");

    return EXIT_SUCCESS;
}

    /*------------------------------------*/
