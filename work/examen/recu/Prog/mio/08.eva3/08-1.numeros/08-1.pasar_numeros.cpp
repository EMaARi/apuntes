#include <stdio.h>
#include <stdlib.h>

char digit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

void binario (unsigned n) {
    if (n > 0)
        binario (n >> 1);
    printf ("%u", n % 2);
}

void hexa (unsigned n) {
    if (n > 0)
        hexa (n / 0x10);
    printf ("%c", digit[n % 0x10]);
}

int main (int argc, char *argv[]) {

    unsigned n = 0,
             x = 0;

    printf ("Introduce numero para hacer el cambio: ");
    scanf (" %u", &n);

    binario (n);
    printf ("\n");

    hexa (n);
    printf ("\n");

    return EXIT_SUCCESS;
}
