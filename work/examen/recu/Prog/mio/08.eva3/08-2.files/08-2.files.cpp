#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x100

int main (int argv, char *argc[]) {

    char palabra [MAX];
    FILE *pf = NULL;

    if (!(pf = fopen ("file.txt", "w"))) {
        fprintf (stderr, "No pude abrir el fichero.\n");
        return EXIT_FAILURE;
    }

    do {
        scanf (" %s", palabra);
        fprintf (pf, "%s ", palabra);
    } while (strcmp (palabra, "fin") != 0);

    fclose (pf);

    return EXIT_SUCCESS;
}
