#include <stdio.h>
#include <stdlib.h>

#define N 10

unsigned suma (unsigned n) {
    unsigned total = 0;
    for (unsigned i = 1; i<=n; i++)
        total += i;

    return total;
}

unsigned suma_r (unsigned n) {
    if (n<1)
        return 0;

    return n + suma (n-1);
}

int main (int argc, char *argv[]) {
    printf ("La suma de los %u primeros numeros: %u\n", N, suma_r (N));

    return EXIT_SUCCESS;
}
