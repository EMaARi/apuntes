#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define M 0x05

struct TStack {
    int data[M];
    int summit;
    int failed;
};

    /*Funciones de la maquina de datos*/

void init (struct TStack *s) {
    s->summit = 0;
    s->failed = 0;
}

void push (struct TStack *s, int ndata) {
    s->failed = 0;
    if (s->summit >= M) {
        s->failed = 1;
        return;
    }
    s->data[s->summit++] = ndata;
}

int pop (struct TStack *s) {
    s->failed = 0;
    if (s->summit <=0){
        s->failed = 1;
        return -666;
    }
    return s->data[--s->summit];
}

    /*Funciones del interfaz*/

void title () {
    system ("clear");
    system ("toilet -fpagga --gay 'PILA'");
    printf ("\n\n");
}

void show_error (const char *mssg) {
    fprintf (stderr, "%s\n", mssg);
}

void show_stack (struct TStack s) {
    title ();
    for (int i = s.summit - 1; i>=0; i++)
        printf ("\t%i\n", s.data[i]);
    printf ("\n\n");
}

int main (int argc, char *argv[]) {
    struct TStack stack;

    init (&stack);

    srand (time (NULL));
    for (int i = 0; i<M; i++) {
        push (&stack, rand () % 10 - 3);
        if (stack.failed)
            show_error ("Stack operation failed.");
    }

    show_stack (stack);

    printf ("\t=> %i\n", pop (&stack));
    if (stack.failed)
        show_error ("Stack pop operation failed.");

    printf ("Pulse una tecla para continuar.");
    getchar ();

    show_stack (stack);

    return EXIT_SUCCESS;
}
