#include <stdio.h>
#include <stdlib.h>

int suma (int op1, int op2) {   //Funcion con codigo
    return op1 + op2;
}

int main (int argc, char *argv[]) {

/*  Palabras:
 *    - int
 *    - *
 *
 *  Tokens:
 *    - int *
 */

    int a, b;       //Declarar un entero

    int *a;         //Puntero entero
    int* b;         //<--- Lo mismo

    int b, *a;      // Entero y puntero entero

    int a[10];      //Un array con 10 celdas reservadas de enteros
    int *b[10];     //Un array que contiene 10 celdas reservadas de punteros enteros

    int (*c)[10]    //Un puntero a un array de 10 enteros

    int suma (int, int); //Una variable suma que contiene codigo ejecutable
    suma (2, 3);    //Codigo
    1008 (2, 3);    //Dirección del codigo

    /*Parametros de la funcion suma*/

    int (*p) (int, int); //Declarar un puntero a la direccion de la funcion suma

    p = &suma;    //Su inicializacion

    (*p) (2, 3);  //Llamar a la funcion

    return EXIT_SUCCESS;
}
