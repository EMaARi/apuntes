#include <stdio.h>
#include <stdlib.h>

#define N 10

unsigned ticket () {
    static int n = 0;

    return ++n;
}

int main (int argc, char *argv[]) {
    for (int i = 0; i<N; i++)
        printf ("Tu ticket es: %u\n", ticket ());

    return EXIT_SUCCESS;
}
