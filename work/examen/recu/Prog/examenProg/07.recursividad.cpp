#include <stdio.h>
#include <stdlib.h>

void binario (unsigned n){
    if(n > 0)
        binario (n >> 1);
    printf ("%u", n % 2);
}

int main (int argc, char *argv[]){
    unsigned decimal = 197;

    binario (decimal);
    printf("\n");

    return EXIT_SUCCESS;
}
