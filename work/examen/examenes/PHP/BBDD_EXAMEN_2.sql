-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: construccion
-- ------------------------------------------------------
-- Server version	8.0.22

CREATE SCHEMA IF NOT EXISTS `construccion` DEFAULT CHARACTER SET utf8 ;
USE `construccion` ;
--
-- Table structure for table `Obras`
--

DROP TABLE IF EXISTS `Obras`;
CREATE TABLE `Obras` (
  `idObras` int NOT NULL,
  `tipo_obra` varchar(45) DEFAULT NULL,
  `fechaComienzo` date DEFAULT NULL,
  PRIMARY KEY (`idObras`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Obras`
--

LOCK TABLES `Obras` WRITE;
/*!40000 ALTER TABLE `Obras` DISABLE KEYS */;
INSERT INTO `Obras` VALUES (0,'Barnizado','2021-05-31'),(1,'Albañileria','2021-05-31'),(2,'Electricidad','2021-05-31'),(3,'Fontaneria','2021-05-31'),(4,'Albañileria','2021-05-31'),(5,'Alicatado','2021-05-31'),(6,'Alicatado','2021-05-31'),(7,'Electricidad','2021-05-31'),(8,'Pintura','2021-05-31'),(9,'Fontaneria','2021-05-31');
/*!40000 ALTER TABLE `Obras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Personal`
--

DROP TABLE IF EXISTS `Personal`;
CREATE TABLE `Personal` (
  `idPersonal` int NOT NULL,
  `nombrePersona` varchar(45) DEFAULT NULL,
  `especialidad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idPersonal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Dumping data for table `Personal`
--

LOCK TABLES `Personal` WRITE;
/*!40000 ALTER TABLE `Personal` DISABLE KEYS */;
INSERT INTO `Personal` VALUES (0,'Maria','Alicatado'),(1,'Ana','Albañileria'),(2,'Lorena','Albañileria'),(3,'Teo','Electricidad'),(4,'Ana','Alicatado'),(5,'Mercedes','Electricidad'),(6,'Mercedes','Pintura'),(7,'Lorena','Alicatado'),(8,'Luis','Alicatado'),(9,'Teo','Alicatado');
/*!40000 ALTER TABLE `Personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tareas`
--

DROP TABLE IF EXISTS `tareas`;
CREATE TABLE `tareas` (
  `idtareas` int NOT NULL,
  `descripción` varchar(45) DEFAULT NULL,
  `duracion` int DEFAULT NULL,
  PRIMARY KEY (`idtareas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Dumping data for table `tareas`
--

LOCK TABLES `tareas` WRITE;
/*!40000 ALTER TABLE `tareas` DISABLE KEYS */;
INSERT INTO `tareas` VALUES (0,'Barnizado',36),(1,'Albañileria',15),(2,'Electricidad',35),(3,'Fontaneria',42),(4,'Albañileria',21),(5,'Alicatado',27),(6,'Alicatado',9),(7,'Electricidad',26),(8,'Pintura',26),(9,'Fontaneria',36);
/*!40000 ALTER TABLE `tareas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trabaja`
--

DROP TABLE IF EXISTS `trabaja`;

CREATE TABLE `trabaja` (
  `Obras_idObras` int NOT NULL,
  `Personal_idPersonal` int NOT NULL,
  `fecha` date DEFAULT NULL,
  `tareas_idtareas` int NOT NULL,
  PRIMARY KEY (`Obras_idObras`,`Personal_idPersonal`,`tareas_idtareas`),
  KEY `fk_Obras_has_Personal_Personal1_idx` (`Personal_idPersonal`),
  KEY `fk_Obras_has_Personal_Obras_idx` (`Obras_idObras`),
  KEY `fk_trabaja_tareas1_idx` (`tareas_idtareas`),
  CONSTRAINT `fk_Obras_has_Personal_Obras` FOREIGN KEY (`Obras_idObras`) REFERENCES `Obras` (`idObras`),
  CONSTRAINT `fk_Obras_has_Personal_Personal1` FOREIGN KEY (`Personal_idPersonal`) REFERENCES `Personal` (`idPersonal`),
  CONSTRAINT `fk_trabaja_tareas1` FOREIGN KEY (`tareas_idtareas`) REFERENCES `tareas` (`idtareas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Dumping data for table `trabaja`
--

LOCK TABLES `trabaja` WRITE;
/*!40000 ALTER TABLE `trabaja` DISABLE KEYS */;
INSERT INTO `trabaja` VALUES (0,3,'2021-06-10',6),(1,7,'2021-06-10',5),(2,3,'2021-06-10',5),(3,6,'2021-06-10',2),(4,9,'2021-06-10',1),(5,2,'2021-06-10',7),(6,1,'2021-06-10',9),(7,3,'2021-06-10',6),(8,1,'2021-06-10',6),(9,2,'2021-06-10',6);
/*!40000 ALTER TABLE `trabaja` ENABLE KEYS */;
UNLOCK TABLES;

