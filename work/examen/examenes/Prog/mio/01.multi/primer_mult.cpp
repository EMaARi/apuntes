#include <stdio.h>
#include <stdlib.h>

#define M 3
#define K 4
#define N 2

int main (int argc, char *argv[]){

    double A[M][K] = {
        { 1,-2, 3, 1},
        {-3, 2, 1,-5},
        {-2, 3, 4,-3}
    };
    double B[K][N] = {
        { 1, 2},
        { 2, 1},
        {-1,-2},
        {-2,-1}
    };
    double C[M][M];

    for (int i=0; i<M; i++)
        for (int j=0; j<N; j++) {
            C[i][j] = 0;
            for (int k=0; k<K; k++)
                C[i][j] += A[i][k] * B[k][j];
        }

    system ("clear");
    for (int i=0; i<M; i++){
        for (int j=0; j<N; j++)
            printf ("%10.2lf", C[i][j]);
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
