#include <stdio.h>
#include <stdlib.h>

#define M 3
#define K 4
#define N 2

void multiplica (double *A, double *B, double *C, int ML, int KL, int NL){
    for (int i=0; i<ML; i++)
        for (int j=0; j<NL; j++) {
            *(C + i * NL + j) = 0;
            for (int k=0; k<KL; k++)
                *(C + i * NL + j) += *(A + i * B[k][j];
        }
}

void print (double *C, int ML, int NL){
    for (int i=0; i<ML; i++){
        for (int j=0; j<NL; j++)
            printf ("%10.2lf", C[i][j]);
        printf ("\n");
    }
}

int main (int argc, char *argv[]){

    double A[M][K] = {
        { 1,-2, 3, 1},
        {-3, 2, 1,-5},
        {-2, 3, 4,-3}
    };
    double B[K][N] = {
        { 1, 2},
        { 2, 1},
        {-1,-2},
        {-2,-1}
    };
    double C[M][N];

    system ("clear");

    multiplica ((double *)A, (double *) B, (double *) C, M, K, N);

    print (A, M, K);
    print (B, K, N);
    print (C, M, N);

    return EXIT_SUCCESS;
}
