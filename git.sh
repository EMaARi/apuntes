#!/bin/bash

function leer_commit (){

    until [[ $res == 'si' ]] || [[ $res == 'yes' ]] || [[ $res == 's' ]] || [[ $res == 'y' ]] || [[ $res == 'S' ]] || [[ $res == Y ]];
    do
        echo -e " Introduzca de nuevo commit: \n "
        read commit
        echo -e "¿Seguro? s/n y/n\n"
        read res
    done

}

function si (){
    echo -e " Para el git commit: \n |[0] Intial commit\n |[1] Otro commit\n"
    read pres
    if [ $pres ==  '1' ];
    then
        leer_commit
        git commit -m "$commit"
    else
        git commit -m "Initial commit"
    fi
}


echo -e "Empezando a subir apuntes.git\n"

git init
git add .

si
echo -e "\n"

echo -e "Se necesita verificación de USUARIO git\n"

git push
echo -e "\n"

echo -e "TERMINADO"
